﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Converters
//  
//  Function： N/A
//  Name： FormLayoutToLayoutConverter
//  
//  Ver       Time                     Author
//  0.10      2021/6/21 11:16:35      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using FruitVentDesign.Enums;
using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;

namespace FruitVentDesign.Converters
{
    public class FormLayoutToLayoutConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            FormLayout layout = (FormLayout)value;
            ItemsPanelTemplate itemsPanelTemplate;
            switch (layout)
            {
                case FormLayout.Horizontal:
                    itemsPanelTemplate = (ItemsPanelTemplate)XamlReader.Parse(@"
                    <ItemsPanelTemplate xmlns=""http://schemas.microsoft.com/winfx/2006/xaml/presentation""
                                xmlns:x=""http://schemas.microsoft.com/winfx/2006/xaml"">
                            <StackPanel/>
                    </ItemsPanelTemplate>");
                    return itemsPanelTemplate;
                case FormLayout.Vertical:
                    itemsPanelTemplate = (ItemsPanelTemplate)XamlReader.Parse(@"
                    <ItemsPanelTemplate xmlns=""http://schemas.microsoft.com/winfx/2006/xaml/presentation""
                                xmlns:x=""http://schemas.microsoft.com/winfx/2006/xaml"">
                            <StackPanel/>
                    </ItemsPanelTemplate>");
                    return itemsPanelTemplate;
                case FormLayout.Inline:
                    itemsPanelTemplate = (ItemsPanelTemplate)XamlReader.Parse(@"
                    <ItemsPanelTemplate xmlns=""http://schemas.microsoft.com/winfx/2006/xaml/presentation""
                                xmlns:x=""http://schemas.microsoft.com/winfx/2006/xaml"">
                            <WrapPanel/>
                    </ItemsPanelTemplate>");
                    return itemsPanelTemplate;
                default:
                    return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
