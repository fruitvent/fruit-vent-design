﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Converters
//  
//  Function： N/A
//  Name： IsShowTableSortConverter
//  
//  Ver       Time                     Author
//  0.10      2021/12/16 17:12:33                   Benedict Deng
//
//  Description:
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Data;

namespace FruitVentDesign.Converters
{
    public class IsShowTableSortConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return values[1] == null
                ? Visibility.Collapsed
                : values[0] is bool canUserSort ? canUserSort ? Visibility.Visible : (object)Visibility.Collapsed : Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
