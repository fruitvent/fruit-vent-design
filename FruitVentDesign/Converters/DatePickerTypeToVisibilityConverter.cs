﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Converters
//  
//  Function： N/A
//  Name： DatePickerTypeToVisibilityConverter
//  
//  Ver       Time                     Author
//  0.10      2023/2/16 8:23:14                   Benedict Deng
//
//  Description:
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

using FruitVentDesign.Enums;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace FruitVentDesign.Converters
{
    public class DatePickerTypeToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is DatePickerType type)
            {
                return DatePickerTypeToVisibility(type);
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        /// <summary>  
        /// DatePickerType 转为 Visibility 
        /// </summary>  
        public static Visibility DatePickerTypeToVisibility(DatePickerType b)
        {
            switch (b)
            {
                case DatePickerType.DateTime:
                    return Visibility.Visible;
                case DatePickerType.Year:
                case DatePickerType.Month:
                case DatePickerType.Date:
                    return Visibility.Collapsed;
                default:
                    return Visibility.Collapsed;
            }
        }
    }
}
