﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Converters
//  
//  Function： N/A
//  Name： HasChildrenMenuBtnToVisibilityConverter
//  
//  Ver       Time                     Author
//  0.10      2021/6/28 18:57:20      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using System;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace FruitVentDesign.Converters
{
    public class HasChildrenMenuBtnToVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values != null)
            {
                if (values.ToArray()[0] is bool s1 && values.ToArray()[1] is bool s2)
                    return BoolToVisibility(s1, s2);
            }
            return values;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return new object[] { false, false };
        }

        public static Visibility BoolToVisibility(bool s1, bool s2)
        {
            if (s1)
            {
                if (s2)
                {
                    return Visibility.Visible;
                }
                else
                {
                    return Visibility.Collapsed;
                }
            }
            else
            {
                return Visibility.Collapsed;
            }
        }
    }
}
