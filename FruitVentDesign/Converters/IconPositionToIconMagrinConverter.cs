﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Converters
//  
//  Function： N/A
//  Name： IconPositionToIconMagrinConverter
//  
//  Ver       Time                     Author
//  0.10      2021/6/16 10:17:49      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace FruitVentDesign.Converters
{
    public class IconPositionToIconMagrinConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Dock dock = (Dock)value;
            Thickness margin = new Thickness(0, 0, 0, 0);
            switch (dock)
            {
                case Dock.Left:
                    margin = new Thickness(-1, 0, 4, 0);
                    return margin;
                case Dock.Right:
                    margin = new Thickness(4, 0, -1, 0);
                    return margin;
                case Dock.Top:
                    margin = new Thickness(0, 0, 0, 0);
                    return margin;
                case Dock.Bottom:
                    margin = new Thickness(0, 0, 0, 0);
                    return margin;
                default:
                    return margin;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
