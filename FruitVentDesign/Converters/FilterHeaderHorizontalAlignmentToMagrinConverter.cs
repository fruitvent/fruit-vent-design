﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Converters
//  
//  Function： N/A
//  Name： FilterHeaderHorizontalAlignmentToMagrinConverter
//  
//  Ver       Time                     Author
//  0.10      2021/6/28 15:13:39      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace FruitVentDesign.Converters
{
    public class FilterHeaderHorizontalAlignmentToMagrinConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            HorizontalAlignment horizontalAlignment = (HorizontalAlignment)value;
            return horizontalAlignment switch
            {
                HorizontalAlignment.Left => new Thickness(0),
                HorizontalAlignment.Right => new Thickness(0),
                HorizontalAlignment.Center => new Thickness(20,0,0,0),
                _ => new Thickness(0),
            };
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
