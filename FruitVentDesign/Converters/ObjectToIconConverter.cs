﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Converters
//  
//  Function： N/A
//  Name： ObjectToIconConverter
//  
//  Ver       Time                     Author
//  0.10      2021/6/16 10:01:12      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Data;

namespace FruitVentDesign.Converters
{
    public class ObjectToIconConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                return Regex.Unescape(StringToUnicode(value.ToString()));
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        /// <summary>  
        /// string will be transformed into string of UniCode
        /// </summary>  
        public static string StringToUnicode(string s)
        {
            if (!string.IsNullOrEmpty(s))
            {
                // let "&#x" will be transformed into "\u" and delete ";"  
                return s.Replace(@"&#x", @"\u").Replace(";", "");
            }
            return s;
        }
    }
}
