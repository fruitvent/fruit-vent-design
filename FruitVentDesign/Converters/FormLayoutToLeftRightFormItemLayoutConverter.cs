﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Converters
//  
//  Function： N/A
//  Name： FormLayoutToLeftRightFormItemLayoutConverter
//  
//  Ver       Time                     Author
//  0.10      2021/6/21 11:20:58      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using FruitVentDesign.Enums;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace FruitVentDesign.Converters
{
    public class FormLayoutToLeftRightFormItemLayoutConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            FormLayout layout = (FormLayout)value;
            return layout switch
            {
                FormLayout.Horizontal => Visibility.Visible,
                FormLayout.Vertical => Visibility.Collapsed,
                FormLayout.Inline => Visibility.Visible,
                _ => Visibility.Collapsed,
            };
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
