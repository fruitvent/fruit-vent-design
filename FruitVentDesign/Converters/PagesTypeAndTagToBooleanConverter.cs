﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Converters
//  
//  Function： N/A
//  Name： PagesTypeAndTagToBooleanConverter
//  
//  Ver       Time                     Author
//  0.10      2021/7/27 10:58:36      Benedict Deng
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using FruitVentDesign.Enums;
using System;
using System.Linq;
using System.Windows.Data;

namespace FruitVentDesign.Converters
{
    public class PagesTypeAndTagToBooleanConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            PagesType pageType = (PagesType)values.ToArray()[0];
            PagesType tag = (PagesType)System.Convert.ToInt32(values.ToArray()[1].ToString());
            if (pageType == tag)
            {
                return true;
            }

            return false;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
