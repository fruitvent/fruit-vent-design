﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Converters
//  
//  Function： N/A
//  Name： IconPositionToPaddingConverter
//  
//  Ver       Time                     Author
//  0.10      2021/6/16 10:13:19      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace FruitVentDesign.Converters
{
    public class IconPositionToPaddingConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Dock dock = (Dock)value;
            Thickness padding = new Thickness(5, 5, 5, 5);
            switch (dock)
            {
                case Dock.Left:
                    padding = new Thickness(15, 4, 15, 4);
                    return padding;
                case Dock.Right:
                    padding = new Thickness(15, 4, 15, 4);
                    return padding;
                case Dock.Top:
                    padding = new Thickness(5, 3, 5, 5);
                    return padding;
                case Dock.Bottom:
                    padding = new Thickness(5, 5, 5, 3);
                    return padding;
                default:
                    return padding;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
