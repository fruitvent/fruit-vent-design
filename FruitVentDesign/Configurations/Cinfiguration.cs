﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Configurations
//  
//  Function： N/A
//  Name： Cinfiguration
//  
//  Ver       Time                     Author
//  0.10      2021/6/16 09:10:12      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FruitVentDesign.Configurations
{
    public static class Cinfiguration
    {
        private static string _themeColor;

        public static string ThemeColor
        {
            get
            {
                if (_themeColor == null)
                {
                    _themeColor = Properties.Settings.Default.ThemeColor;
                }
                return _themeColor;
            }
            set
            {
                _themeColor = value ?? "";

                Properties.Settings.Default.ThemeColor = _themeColor;
                Properties.Settings.Default.Save();
            }
        }
    }
}
