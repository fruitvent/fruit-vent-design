﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Enums
//  
//  Function： N/A
//  Name： 
//  
//  Ver       Time                     Author
//  0.10      2021/6/16 14:39:17      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace FruitVentDesign.Enums
{
    public enum ThemeKind
    {
        Blue,
        Orange,
        SkyBlue
    }

    public enum FormLayout
    {
        Horizontal,
        Vertical,
        Inline
    }

    public enum FormItemLabelPosition
    {
        Left,
        Right
    }

    public enum ButtonType
    {
        Default,
        Primary,
        Primary_Success,
        Primary_Danger,
        Text,
        Link,
        Link_Danger
    }

    public enum CheckBoxType
    {
        Default,
        Rimless,
        FilledCircle,
        Slider
    }

    public enum RadioType
    {
        Default,
        ButtonDefault,
        ButtonSolid,
        Color,
    }

    public enum PagesType
    {
        All,
        CurrentPage,
        PageRangeMultiFrame,
        PageRange
    }

    public enum DatePickerType
    {
        Year,
        Month,
        Date,
        DateTime,
    }
}
