﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.UtilWindows
//  
//  Function： N/A
//  Name： EnterWindow
//  
//  Ver       Time                     Author
//  0.10      2021/6/17 10:41:56      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using System.Windows;

namespace FruitVentDesign.UtilWindows
{
    /// <summary>
    /// EnterWindow.xaml 的交互逻辑
    /// </summary>
    public partial class EnterWindow : Windows.Window
    {
        public EnterWindow()
        {
            InitializeComponent();
        }

        public string Text { get; private set; }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Text = messageTextBox.Text;

            DialogResult = true;
        }
    }
}
