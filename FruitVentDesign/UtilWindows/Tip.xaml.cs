﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FruitVentDesign.UtilWindows
{
    /// <summary>
    /// Tip.xaml 的交互逻辑
    /// </summary>
    public partial class Tip : UserControl
    {
        public Tip(string message)
        {
            InitializeComponent();

            Tip_Text.Text = message;
        }
    }
}
