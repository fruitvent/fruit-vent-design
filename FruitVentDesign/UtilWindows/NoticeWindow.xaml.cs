﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.UtilWindows
//  
//  Function： N/A
//  Name： NoticeWindow
//  
//  Ver       Time                     Author
//  0.10      2021/6/17 10:47:50     FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using System;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace FruitVentDesign.UtilWindows
{
    /// <summary>
    /// NoticeWindow.xaml 的交互逻辑
    /// </summary>
    public partial class NoticeWindow : Windows.Window
    {
        public NoticeWindow()
        {
            InitializeComponent();
        }

        public NoticeWindow(string message) : this()
        {
            messageTextBox.Text = message;
            Task.Factory.StartNew(delegate
            {
                int seconds = 30;//通知持续30s后消失
                System.Threading.Thread.Sleep(TimeSpan.FromSeconds(seconds));
                //Invoke到主进程中去执行
                this.Dispatcher.Invoke(delegate
                {
                    this.IsEnabled = false;

                    mGrid.OpacityMask = this.Resources["ClosedBrush"] as LinearGradientBrush;
                    Storyboard std = this.Resources["ClosedStoryboard"] as Storyboard;
                    std.Completed += delegate { this.Close(); };

                    std.Begin();
                });
            });
        }
    }
}
