﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.UtilWindows
//  
//  Function： N/A
//  Name： ReadOnlyWindow
//  
//  Ver       Time                     Author
//  0.10      2021/6/17 11:34:44     FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using System.Windows;

namespace FruitVentDesign.UtilWindows
{
    /// <summary>
    /// ReadOnlyWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ReadOnlyWindow : Windows.Window
    {
        public ReadOnlyWindow()
        {
            InitializeComponent();
        }

        public ReadOnlyWindow(string message) : this()
        {
            messageTextBox.Text = message;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
