﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.UtilWindows
//  
//  Function： N/A
//  Name： ThemeColorWindow
//  
//  Ver       Time                     Author
//  0.10      2021/6/16 17:08:39     FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using FruitVentDesign.Controls;
using FruitVentDesign.Enums;
using FruitVentDesign.Utils;
using FruitVentDesign.Windows;
using System;
using System.Diagnostics;

namespace FruitVentDesign.UtilWindows
{
    /// <summary>
    /// ThemeColorWindow.xaml 的交互逻辑
    /// </summary>
    public partial class ThemeColorWindow : Window
    {
        public ThemeColorWindow()
        {
            InitializeComponent();
            DataContext = this;
        }

        /// <summary>
        /// 主题
        /// </summary>
        private ThemeKind _themeKind;
        public ThemeKind ThemeKind
        {
            get
            {
                switch (Configurations.Cinfiguration.ThemeColor)
                {
                    case "Blue":
                        _themeKind = ThemeKind.Blue;
                        break;
                    default:
                        Debugger.Break();
                        break;
                }
                return _themeKind;
            }
            set
            {
                _themeKind = value;
                OnPropertyChanged(nameof(ThemeKind));
                Configurations.Cinfiguration.ThemeColor = _themeKind.ToString();
                ThemeAssist.ChangeTheme(_themeKind);
                Trace.WriteLine(_themeKind.ToString());
            }
        }

        /// <summary>
        /// 切换主题
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ThemeColorRadio_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Radio radio= (Radio)sender;
            string tag = radio.Tag.ToString();
            ThemeKind themeKind = (ThemeKind)System.Convert.ToInt32(tag);
            ThemeKind = themeKind;
            this.Close();
        }
    }
}
