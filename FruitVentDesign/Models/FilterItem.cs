﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Models
//  
//  Function： N/A
//  Name： FilterItem
//  
//  Ver       Time                     Author
//  0.10      2021/6/27 22:05:32      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using System;
using System.ComponentModel;

namespace FruitVentDesign.Models
{
    public class FilterItem : INotifyPropertyChanged
	{
		public FilterItem()
		{
			IsPassed = DefaultIsPassed;
		}

		private bool _isSelected = false;
		public bool IsSelected
		{
			get { return _isSelected; }
			set
			{
				if (_isSelected != value)
				{
					_isSelected = value;
					OnPropertyChanged(nameof(IsSelected));
				}
			}
		}

		private string _displayText;
		public string DisplayText
		{
			get { return _displayText; }
			set
			{
				if (_displayText != value)
				{
					_displayText = value;
					OnPropertyChanged(nameof(DisplayText));
				}
			}
		}

		public Func<string, object, bool> IsPassed { get; set; }

		private bool DefaultIsPassed(string displayText, object actualValue)
		{
			if (displayText == null) { return true; }
			return displayText.Equals(actualValue);
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged(string propertyName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
