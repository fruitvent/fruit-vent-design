﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Models
//  
//  Function： N/A
//  Name： ThemeColor
//  
//  Ver       Time                     Author
//  0.10      2021/6/16 10:22:11      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using FruitVentDesign.Enums;
using System.ComponentModel;
using System.Diagnostics;

namespace FruitVentDesign.Models
{
    public class ThemeColor : INotifyPropertyChanged
    {
        private ThemeKind _themeKind;

        public ThemeKind ThemeKind
        {
            get
            {
                switch (FruitVentDesign.Configurations.Cinfiguration.ThemeColor)
                {
                    case "Blue":
                        _themeKind = ThemeKind.Blue;
                        break;
                    case "Orange":
                        _themeKind = ThemeKind.Orange;
                        break;
                    case "SkyBlue":
                        _themeKind = ThemeKind.SkyBlue;
                        break;
                    default:
                        Debugger.Break();
                        break;
                }
                return _themeKind;
            }
            set
            {
                _themeKind = value;
                OnPropertyChanged(nameof(ThemeKind));
                FruitVentDesign.Configurations.Cinfiguration.ThemeColor = value.ToString();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
