﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Models
//  
//  Function： N/A
//  Name： DateTimeChangeEventArgs
//  
//  Ver       Time                     Author
//  0.10      2023/2/14 14:18:31                   Benedict Deng
//
//  Description:
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;

namespace FruitVentDesign.Models
{
    public class DateTimeChangeEventArgs : EventArgs
    {
        public DateTimeChangeEventArgs(object data)
        {
            Data = data;    
        }

        public object Data { get; set; }
    }
}
