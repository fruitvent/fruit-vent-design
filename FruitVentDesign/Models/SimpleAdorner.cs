﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Models
//  
//  Function： N/A
//  Name： SimpleAdorner
//  
//  Ver       Time                     Author
//  0.10      2023/2/2 9:31:47                   Benedict Deng
//
//  Description:
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace FruitVentDesign.Models
{
    public class SimpleAdorner : Adorner
    {
        private UIElement _child;
        public SimpleAdorner(UIElement adornedElement) : base(adornedElement)
        {
        }
        //Adorner 直接继承自 FrameworkElement，
        //没有Content和Child属性，
        //自己添加一个，方便向其中添加我们的控件
        public UIElement Child
        {
            get => _child;
            set
            {
                if (value == null)
                {
                    RemoveVisualChild(_child);
                }
                else
                {
                    AddVisualChild(value);
                }
                _child = value;
            }
        }
        //重写VisualChildrenCount 表示此控件只有一个子控件
        protected override int VisualChildrenCount => 1;
        //控件计算大小的时候，我们在装饰层上添加的控件也计算一下大小
        protected override Size ArrangeOverride(Size finalSize)
        {
            _child?.Arrange(new Rect(finalSize));
            return finalSize;
        }
        //重写GetVisualChild,返回我们添加的控件
        protected override Visual GetVisualChild(int index)
        {
            if (index == 0 && _child != null) return _child;
            return base.GetVisualChild(index);
        }
    }
}
