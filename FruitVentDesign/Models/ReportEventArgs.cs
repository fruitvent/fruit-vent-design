﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Models
//  
//  Function： N/A
//  Name： ReportEventArgs
//  
//  Ver       Time                     Author
//  0.10      2021/6/17 14:18:21      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using System;

namespace FruitVentDesign.Models
{
    public class ReportEventArgs : EventArgs
    {
        public static ReportEventArgs Over { get; } = new ReportEventArgs(1, 1, true);

        public ReportEventArgs(int done, int total, bool isOver)
        {
            Done = done;
            Total = total;
            IsOver = isOver;
        }

        public bool IsOver { get; }

        public int Done { get; }

        public int Total { get; }

    }
}
