﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Primitives
//  
//  Function： N/A
//  Name： WindowExBase
//  
//  Ver       Time                     Author
//  0.10      2021/6/16 11:39:55      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using FruitVentDesign.Commands;
using FruitVentDesign.UtilWindows;
using System;
using System.Windows;
using System.Windows.Input;

namespace FruitVentDesign.Primitives
{
    public class WindowExBase : Window
    {
        #region CanSwitchTheme
        /// <summary>
        /// theme button is show or no
        /// </summary>
        public static readonly DependencyProperty CanSwitchThemeProperty = DependencyProperty.Register(
             "CanSwitchTheme",
             typeof(bool),
             typeof(WindowExBase),
             new FrameworkPropertyMetadata(false)
             );

        public bool CanSwitchTheme
        {
            get => (bool)GetValue(CanSwitchThemeProperty);
            set => SetValue(CanSwitchThemeProperty, value);
        }
        #endregion

        #region CornerRadius
        /// <summary>
        /// minimize button is show or no
        /// </summary>
        public static readonly DependencyProperty CornerRadiusProperty = DependencyProperty.Register(
             "CornerRadius",
             typeof(int),
             typeof(WindowExBase),
             new FrameworkPropertyMetadata(0)
             );

        public int CornerRadius
        {
            get => (int)GetValue(CornerRadiusProperty);
            set => SetValue(CornerRadiusProperty, value);
        }

        public static void SetCornerRadius(DependencyObject element, int value)
        {
            element.SetValue(CornerRadiusProperty, value);
        }

        public static int GetCornerRadius(DependencyObject element)
        {
            return (int)element.GetValue(CornerRadiusProperty);
        }
        #endregion

        public WindowExBase()
        {
            CommandBindings.Add(new CommandBinding(SystemCommands.CloseWindowCommand, CloseWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.MaximizeWindowCommand, MaximizeWindow, CanResizeWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.MinimizeWindowCommand, MinimizeWindow, CanMinimizeWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.RestoreWindowCommand, RestoreWindow, CanResizeWindow));
            CommandBindings.Add(new CommandBinding(SystemCommands.ShowSystemMenuCommand, ShowSystemMenu));
            CommandBindings.Add(new CommandBinding(RoutedCommands.ShowThemeWindowCommand, ShowThemeWindow, CanShowThemeWindow));
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            if (e.ButtonState == MouseButtonState.Pressed)
                DragMove();
        }

        protected override void OnContentRendered(EventArgs e)
        {
            base.OnContentRendered(e);
            if (SizeToContent == SizeToContent.WidthAndHeight)
                InvalidateMeasure();
        }


        #region Window Commands

        private void CanShowThemeWindow(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CanResizeWindow(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ResizeMode == ResizeMode.CanResize || ResizeMode == ResizeMode.CanResizeWithGrip;
        }

        private void CanMinimizeWindow(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ResizeMode != ResizeMode.NoResize;
        }

        private void CloseWindow(object sender, ExecutedRoutedEventArgs e)
        {
            this.Close();
        }

        private void MaximizeWindow(object sender, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MaximizeWindow(this);
        }

        private void MinimizeWindow(object sender, ExecutedRoutedEventArgs e)
        {
            SystemCommands.MinimizeWindow(this);
        }

        private void RestoreWindow(object sender, ExecutedRoutedEventArgs e)
        {
            SystemCommands.RestoreWindow(this);
        }

        private void ShowSystemMenu(object sender, ExecutedRoutedEventArgs e)
        {
            var element = e.OriginalSource as FrameworkElement;
            if (element == null)
                return;

            var point = WindowState == WindowState.Maximized ? new Point(0, element.ActualHeight)
                : new Point(Left + BorderThickness.Left, element.ActualHeight + Top + BorderThickness.Top);
            point = element.TransformToAncestor(this).Transform(point);
            SystemCommands.ShowSystemMenu(this, point);
        }

        private void ShowThemeWindow(object sender, ExecutedRoutedEventArgs e)
        {
            ThemeColorWindow themeColorWindow = new ThemeColorWindow() { Owner = this };
            themeColorWindow.ShowDialog();
        }

        #endregion


        //public WindowExBase()
        //{
        //    Loaded += WindowExBase_Loaded;
        //    SizeChanged += WindowExBase_SizeChanged;
        //}

        //private void WindowExBase_SizeChanged(object sender, SizeChangedEventArgs e)
        //{
        //    SetWindowNoBorder(this);
        //}

        //private void WindowExBase_Loaded(object sender, RoutedEventArgs e)
        //{
        //    SetWindowNoBorder(this);
        //}

        ///// <summary> 
        ///// 带有外边框和标题的windows的样式 
        ///// </summary> 
        //public const int WS_CAPTION = 0x00C00000;
        //public const int WS_CAPTION_2 = 0X00C0000;
        //// public const long WS_BORDER = 0X0080000L; 

        ///// <summary> 
        ///// window 扩展样式 分层显示 
        ///// </summary> 
        //public const int WS_EX_LAYERED = 0x00080000;
        //public const int WS_CHILD = 0x40000000;

        ///// <summary> 
        ///// 带有alpha的样式 
        ///// </summary> 
        //public const int LWA_ALPHA = 0x00000002;

        ///// <summary> 
        ///// 颜色设置 
        ///// </summary> 
        //public const int LWA_COLORKEY = 0x00000001;

        ///// <summary> 
        ///// window的基本样式 
        ///// </summary> 
        //public const int GWL_STYLE = -16;

        ///// <summary> 
        ///// window的扩展样式 
        ///// </summary> 
        //public const int GWL_EXSTYLE = -20;

        ///// <summary> 
        ///// 设置窗体的样式 
        ///// </summary> 
        ///// <param name="handle">操作窗体的句柄</param> 
        ///// <param name="oldStyle">进行设置窗体的样式类型.</param> 
        ///// <param name="newStyle">新样式</param> 
        //[System.Runtime.InteropServices.DllImport("User32.dll")]
        //public static extern void SetWindowLong(IntPtr handle, int oldStyle, int newStyle);

        ///// <summary> 
        ///// 获取窗体指定的样式. 
        ///// </summary> 
        ///// <param name="handle">操作窗体的句柄</param> 
        ///// <param name="style">要进行返回的样式</param> 
        ///// <returns>当前window的样式</returns> 
        //[System.Runtime.InteropServices.DllImport("User32.dll")]
        //public static extern int GetWindowLong(IntPtr handle, int style);

        ///// <summary> 
        ///// 设置窗体的工作区域. 
        ///// </summary> 
        ///// <param name="handle">操作窗体的句柄.</param> 
        ///// <param name="handleRegion">操作窗体区域的句柄.</param> 
        ///// <param name="regraw">if set to <c>true</c> [regraw].</param> 
        ///// <returns>返回值</returns> 
        //[System.Runtime.InteropServices.DllImport("User32.dll")]
        //public static extern int SetWindowRgn(IntPtr handle, IntPtr handleRegion, bool regraw);

        ///// <summary> 
        ///// 创建带有圆角的区域. 
        ///// </summary> 
        ///// <param name="x1">左上角坐标的X值.</param> 
        ///// <param name="y1">左上角坐标的Y值.</param> 
        ///// <param name="x2">右下角坐标的X值.</param> 
        ///// <param name="y2">右下角坐标的Y值.</param> 
        ///// <param name="width">圆角椭圆的width.</param> 
        ///// <param name="height">圆角椭圆的height.</param> 
        ///// <returns>hRgn的句柄</returns> 
        //[System.Runtime.InteropServices.DllImport("gdi32.dll")]
        //public static extern IntPtr CreateRoundRectRgn(int x1, int y1, int x2, int y2, int width, int height);

        ///// <summary> 
        ///// Sets the layered window attributes. 
        ///// </summary> 
        ///// <param name="handle">要进行操作的窗口句柄</param> 
        ///// <param name="colorKey">RGB的值</param> 
        ///// <param name="alpha">Alpha的值，透明度</param> 
        ///// <param name="flags">附带参数</param> 
        ///// <returns>true or false</returns> 
        //[System.Runtime.InteropServices.DllImport("User32.dll")]
        //public static extern bool SetLayeredWindowAttributes(IntPtr handle, int colorKey, byte alpha, int flags);

        ///// <summary>
        ///// 设置窗体为无边框风格
        ///// </summary>
        ///// <param name="hWnd"></param>
        //public static void SetWindowNoBorder(Window window)
        //{
        //    // 获取窗体句柄  
        //    IntPtr hwnd = new System.Windows.Interop.WindowInteropHelper(window).Handle;
        //    int oldstyle = GetWindowLong(hwnd, GWL_STYLE);
        //    SetWindowLong(hwnd, GWL_STYLE, oldstyle & (~(WS_CAPTION | WS_CAPTION_2)));
        //    SetWindowLong(hwnd, GWL_EXSTYLE, WS_CHILD);
        //    // 设置窗体为透明窗体  
        //    SetLayeredWindowAttributes(hwnd, 1 | 2 << 8 | 3 << 16, 0, LWA_ALPHA);
        //    // 创建圆角窗体  12 这个值可以根据自身项目进行设置  
        //    SetWindowRgn(hwnd, CreateRoundRectRgn(0, 0, Convert.ToInt32(window.ActualWidth), Convert.ToInt32(window.ActualHeight), GetCornerRadius(window), GetCornerRadius(window)), true);  
        //}
    }
}
