﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Primitives
//  
//  Function： N/A
//  Name： ButtonExBase
//  
//  Ver       Time                     Author
//  0.10      2021/6/16 08:27:10      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace FruitVentDesign.Primitives
{
    public class ButtonExBase : ButtonBase
    {
        private static readonly Brush iconColor = new SolidColorBrush(Color.FromRgb(37, 37, 37));
        private static readonly Brush fontColor = new SolidColorBrush(Color.FromRgb(37, 37, 37));
        private static readonly Brush borderColor = new SolidColorBrush(Color.FromRgb(37, 37, 37));

        #region icon property
        /// <summary>
        /// icon 
        /// 图标
        /// </summary>
        public static readonly DependencyProperty IconProperty = 
            DependencyProperty.Register("Icon", typeof(object), typeof(ButtonExBase), new PropertyMetadata(null));

        public object Icon
        {
            get { return (object)GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }

        #region iconTemplate property
        /// <summary>
        /// icon template
        /// 图标模板
        /// </summary>
        public static readonly DependencyProperty IconTemplateProperty =
            DependencyProperty.Register("IconTemplate", typeof(ControlTemplate), typeof(ButtonExBase), new PropertyMetadata(null));

        public ControlTemplate IconTemplate
        {
            get { return (ControlTemplate)GetValue(IconTemplateProperty); }
            set { SetValue(IconTemplateProperty, value); }
        }
        #endregion

        /// <summary>
        /// icon size
        /// 图标大小
        /// </summary>
        public static readonly DependencyProperty IconSizeProperty =
            DependencyProperty.Register("IconSize", typeof(double), typeof(ButtonExBase), new PropertyMetadata(18D));

        public double IconSize
        {
            get { return (double)GetValue(IconSizeProperty); }
            set { SetValue(IconSizeProperty, value); }
        }

        /// <summary>
        /// icon margin
        /// 图标间距
        /// </summary>
        public static readonly DependencyProperty IconMarginProperty = 
            DependencyProperty.Register("IconMargin", typeof(Thickness), typeof(ButtonExBase), new PropertyMetadata(new Thickness(0)));
        
        public Thickness IconMargin
        {
            get { return (Thickness)GetValue(IconMarginProperty); }
            set { SetValue(IconMarginProperty, value); }
        }

        /// <summary>
        /// icon position
        /// 图标位置
        /// </summary>
        public static readonly DependencyProperty IconPositionProperty =
           DependencyProperty.Register("IconPosition", typeof(Dock), typeof(ButtonExBase), new PropertyMetadata(Dock.Left));
        
        public Dock IconPosition
        {
            get { return (Dock)GetValue(IconPositionProperty); }
            set { SetValue(IconPositionProperty, value); }
        }

        /// <summary>
        /// Whether to allow rotation
        /// 是否允许图标旋转
        /// </summary>
        public static readonly DependencyProperty AllowRotationProperty = 
            DependencyProperty.Register("AllowRotation", typeof(bool), typeof(ButtonExBase), new PropertyMetadata(false));
        
        public bool AllowRotation
        {
            get { return (bool)GetValue(AllowRotationProperty); }
            set { SetValue(AllowRotationProperty, value); }
        }

        /// <summary>
        /// icon foreground property
        /// 图标颜色（默认）
        /// </summary>
        public static readonly DependencyProperty IconForegroundProperty =
            DependencyProperty.Register("IconForeground", typeof(Brush), typeof(ButtonExBase), new PropertyMetadata(iconColor));
        
        public Brush IconForeground
        {
            get { return (Brush)GetValue(IconForegroundProperty); }
            set { SetValue(IconForegroundProperty, value); }
        }

        /// <summary>
        /// icon foreground when mouse over
        /// 图标颜色（鼠标经过）
        /// </summary>
        public static readonly DependencyProperty MouseOverIconForegroundProperty =
            DependencyProperty.Register("MouseOverIconForeground", typeof(Brush), typeof(ButtonExBase), new PropertyMetadata(iconColor));

        public Brush MouseOverIconForeground
        {
            get { return (Brush)GetValue(MouseOverIconForegroundProperty); }
            set { SetValue(MouseOverIconForegroundProperty, value); }
        }

        /// <summary>
        /// icon foreground when mouse pressed
        /// 图标颜色（鼠标按下）
        /// </summary>
        public static readonly DependencyProperty PressedIconForegroundProperty =
            DependencyProperty.Register("PressedIconForeground", typeof(Brush), typeof(ButtonExBase), new PropertyMetadata(iconColor));

        public Brush PressedIconForeground
        {
            get { return (Brush)GetValue(PressedIconForegroundProperty); }
            set { SetValue(PressedIconForegroundProperty, value); }
        }
        #endregion

        #region corner radius
        /// <summary>
        /// corner radius
        /// 按钮圆角大小,左上，右上，右下，左下
        /// </summary>
        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register("CornerRadius", typeof(CornerRadius), typeof(ButtonExBase), new PropertyMetadata(new CornerRadius(3)));
        
        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }
        #endregion

        #region content decorations
        /// <summary>
        /// content decorations
        /// 文字修饰（下划线，中划线， 基线， 上划线，空心字、立体字、划线字、阴影字、加粗、倾斜等）
        /// </summary>
        public static readonly DependencyProperty ContentDecorationsProperty = DependencyProperty.Register(
            "ContentDecorations", typeof(TextDecorationCollection), typeof(ButtonExBase), new PropertyMetadata(null));

        public TextDecorationCollection ContentDecorations
        {
            get { return (TextDecorationCollection)GetValue(ContentDecorationsProperty); }
            set { SetValue(ContentDecorationsProperty, value); }
        }
        #endregion

        #region foreground
        /// <summary>
        /// foreground when mouse over
        /// 文本内容字体颜色（鼠标经过）
        /// </summary>
        public static readonly DependencyProperty MouseOverForegroundProperty =
           DependencyProperty.Register("MouseOverForeground", typeof(Brush), typeof(ButtonExBase), new PropertyMetadata(fontColor));
        
        public Brush MouseOverForeground
        {
            get { return (Brush)GetValue(MouseOverForegroundProperty); }
            set { SetValue(MouseOverForegroundProperty, value); }
        }

        /// <summary>
        /// foreground when mouse pressed
        /// 文本内容字体颜色（鼠标按下）
        /// </summary>
        public static readonly DependencyProperty PressedForegroundProperty =
            DependencyProperty.Register("PressedForeground", typeof(Brush), typeof(ButtonExBase), new PropertyMetadata(fontColor));
        
        public Brush PressedForeground
        {
            get { return (Brush)GetValue(PressedForegroundProperty); }
            set { SetValue(PressedForegroundProperty, value); }
        }
        #endregion

        #region background
        /// <summary>
        /// background when mouse over
        /// 按钮颜色（鼠标经过）
        /// </summary>
        public static readonly DependencyProperty MouseOverBackgroundProperty =
            DependencyProperty.Register("MouseOverBackground", typeof(Brush), typeof(ButtonExBase), new PropertyMetadata(Brushes.White));
        
        public Brush MouseOverBackground
        {
            get { return (Brush)GetValue(MouseOverBackgroundProperty); }
            set { SetValue(MouseOverBackgroundProperty, value); }
        }

        /// <summary>
        /// background when mouse pressed
        /// 按钮颜色（鼠标按下）
        /// </summary>
        public static readonly DependencyProperty PressedBackgroundProperty =
            DependencyProperty.Register("PressedBackground", typeof(Brush), typeof(ButtonExBase), new PropertyMetadata(Brushes.White));
        
        public Brush PressedBackground
        {
            get { return (Brush)GetValue(PressedBackgroundProperty); }
            set { SetValue(PressedBackgroundProperty, value); }
        }
        #endregion

        #region border brush
        /// <summary>
        /// border brush when mouse over
        /// 边框颜色（鼠标经过）
        /// </summary>
        public static readonly DependencyProperty MouseOverBorderBrushProperty =
          DependencyProperty.Register("MouseOverBorderBrush", typeof(Brush), typeof(ButtonExBase), new PropertyMetadata(borderColor));
        
        public Brush MouseOverBorderBrush
        {
            get { return (Brush)GetValue(MouseOverBorderBrushProperty); }
            set { SetValue(MouseOverBorderBrushProperty, value); }
        }

        /// <summary>
        /// border brush when mouse pressed
        /// 边框颜色（鼠标按下）
        /// </summary>
        public static readonly DependencyProperty PressedBorderBrushProperty =
            DependencyProperty.Register("PressedBorderBrush", typeof(Brush), typeof(ButtonExBase), new PropertyMetadata(borderColor));
        
        public Brush PressedBorderBrush
        {
            get { return (Brush)GetValue(PressedBorderBrushProperty); }
            set { SetValue(PressedBorderBrushProperty, value); }
        }
        #endregion
    }
}
