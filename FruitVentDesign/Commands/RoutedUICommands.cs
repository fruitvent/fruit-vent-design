﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Commands
//  
//  Function： N/A
//  Name： RoutedUICommands
//  
//  Ver       Time                     Author
//  0.10      2021/6/18 21:47:11      FruitVent
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using Microsoft.Win32;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace FruitVentDesign.Commands
{
    public static class RoutedUICommands
    {
        static RoutedUICommands()
        {
            ClearTextCommandBinding.Executed += ClearButtonClick;
            OpenFileCommandBinding.Executed += OpenFileButtonClick;
            OpenFolderCommandBinding.Executed += OpenFolderButtonClick;
            SaveFileCommandBinding.Executed += SaveFileButtonClick;
            DoFilterCommandBinding.Executed += DoFilterButtonClick;
        }

        #region ClearTextCommand 清除输入框Text事件命令
        /// <summary>
        /// 清除输入框Text事件命令，需要使用AllowClear绑定命令
        /// </summary>
        public static RoutedUICommand ClearTextCommand { get; private set; } = new RoutedUICommand();

        /// <summary>
        /// ClearTextCommand绑定事件
        /// </summary>
        public static readonly CommandBinding ClearTextCommandBinding = new CommandBinding(ClearTextCommand);

        /// <summary>
        /// 清除输入框文本值
        /// </summary>
        private static void ClearButtonClick(object sender, ExecutedRoutedEventArgs e)
        {
            if (!(e.Parameter is FrameworkElement element)) { return; }

            if (element is TextBox textBox)
            {
                textBox.Clear();
                if (element is Controls.PromptInput input)
                {
                    input.Execute();
                }
            }
            if (element is PasswordBox passwordBox)
            {
                passwordBox.Clear();
            }
            if (element is ComboBox comboBox)
            {
                comboBox.SelectedItem = null;
                comboBox.Text = null;
                comboBox.SelectedValue = null;
                if (element is Controls.Select select)
                {
                    select.PreviousSelectedItem = null;
                }
            }
            if (element is DatePicker datePicker)
            {
                datePicker.SelectedDate = null;
                datePicker.Text = string.Empty;
            }

            _ = element.Focus();
        }
        #endregion

        #region OpenFileCommand 选择文件命令
        /// <summary>
        /// 选择文件命令，需要使用IsClearTextButtonBehaviorEnabledChanged绑定命令
        /// </summary>
        public static RoutedUICommand OpenFileCommand { get; private set; } = new RoutedUICommand();

        /// <summary>
        /// OpenFileCommand绑定事件
        /// </summary>
        public static readonly CommandBinding OpenFileCommandBinding = new CommandBinding(OpenFileCommand);

        /// <summary>
        /// 执行OpenFileCommand
        /// </summary>
        private static void OpenFileButtonClick(object sender, ExecutedRoutedEventArgs e)
        {
            if (!(e.Parameter is FrameworkElement element)) { return; }
            if (!(element is TextBox txt)) { return; }
            string filter = txt.Tag == null ? "所有文件(*.*)|*.*" : txt.Tag.ToString();
            if (filter.Contains(".bin"))
            {
                filter += "|所有文件(*.*)|*.*";
            }
            if (txt == null) return;
            OpenFileDialog fd = new OpenFileDialog();
            fd.Title = "请选择文件";
            //“图像文件(*.bmp, *.jpg)|*.bmp;*.jpg|所有文件(*.*)|*.*”
            fd.Filter = filter;
            fd.FileName = txt.Text.Trim();
            if (fd.ShowDialog() == true)
            {
                txt.Text = fd.FileName;
            }
            element.Focus();
        }
        #endregion

        #region OpenFolderCommand 选择文件夹命令
        /// <summary>
        /// 选择文件夹命令
        /// </summary>
        public static RoutedUICommand OpenFolderCommand { get; private set; } = new RoutedUICommand();

        /// <summary>
        /// OpenFolderCommand绑定事件
        /// </summary>
        public static readonly CommandBinding OpenFolderCommandBinding = new CommandBinding(OpenFolderCommand);

        /// <summary>
        /// 执行OpenFolderCommand
        /// </summary>
        private static void OpenFolderButtonClick(object sender, ExecutedRoutedEventArgs e)
        {
            if (!(e.Parameter is FrameworkElement element)) { return; }
            if (!(element is TextBox txt)) { return; }
            System.Windows.Forms.FolderBrowserDialog fd = new System.Windows.Forms.FolderBrowserDialog
            {
                Description = "请选择文件路径",
                SelectedPath = txt.Text.Trim()
            };
            if (fd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txt.Text = fd.SelectedPath;
            }
            element.Focus();
        }
        #endregion

        #region SaveFileCommand 选择文件保存路径及名称
        /// <summary>
        /// 选择文件保存路径及名称
        /// </summary>
        public static RoutedUICommand SaveFileCommand { get; private set; } = new RoutedUICommand();

        /// <summary>
        /// SaveFileCommand绑定事件
        /// </summary>
        public static readonly CommandBinding SaveFileCommandBinding = new CommandBinding(SaveFileCommand);

        /// <summary>
        /// 执行OpenFileCommand
        /// </summary>
        private static void SaveFileButtonClick(object sender, ExecutedRoutedEventArgs e)
        {
            if (!(e.Parameter is FrameworkElement element)) { return; }
            if (!(element is TextBox txt)) { return; }
            System.Windows.Forms.SaveFileDialog fd = new System.Windows.Forms.SaveFileDialog
            {
                Title = "文件保存路径",
                Filter = "所有文件(*.*)|*.*",
                FileName = txt.Text.Trim()
            };
            if (fd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txt.Text = fd.FileName;
            }
            element.Focus();
        }
        #endregion

        #region DoFilterCommand 筛选
        /// <summary>
        /// 筛选
        /// </summary>
        public static RoutedUICommand DoFilterCommand { get; private set; } = new RoutedUICommand();

        /// <summary>
        /// DoFilterCommand绑定事件
        /// </summary>
        public static readonly CommandBinding DoFilterCommandBinding = new CommandBinding(DoFilterCommand);

        private static Controls.Table tablet = null;

        /// <summary>
        /// 执行DoFilterCommand
        /// </summary>
        private static void DoFilterButtonClick(object sender, ExecutedRoutedEventArgs e)
        {
            if (!(e.Parameter is FrameworkElement element)) { return; }

            if (!(element is Controls.Table table)) { return; }

            tablet = table;

            if (!(sender is Controls.Button button)) { return; }

            if (!(button.Tag is Controls.ToggleButtonEx t)) { return; }

            t.IsChecked = false;

            if (!(button.Parent is DockPanel dockPanel)) {return; }
            if (!(dockPanel.Parent is Border border)) { return; }
            if (!(border.Parent is Grid grid1)) { return; }
            if (!(grid1.Parent is Grid grid2)) { return; }
            if (!(grid2.Parent is Popup popup)) { return; }

            popup.Closed += Popup_Closed;
        }

        private static void Popup_Closed(object sender, System.EventArgs e)
        {
            if (sender is Popup popup)
            {
                if (popup.Tag is Controls.FilterHeader header)
                {
                    header.UpdateHasSelectedFilterItems();
                }
            }
            tablet.DoFilter();
        }
        #endregion
    }
}
