﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Windows
//  
//  Function： N/A
//  Name： Window
//  
//  Ver       Time                     Author
//  0.10      2021/6/16 11:38:05      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using FruitVentDesign.Primitives;
using System.ComponentModel;
using System.Windows;

namespace FruitVentDesign.Windows
{
    public class Window : WindowExBase, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        static Window()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Window), new FrameworkPropertyMetadata(typeof(Window)));
        }

        /// <summary>
        /// 是否有记录
        /// </summary>
        public bool HasRcnormal { get; set; } = false;

        /// <summary>
        /// 定义一个全局rect记录还原状态下窗口的位置和大小。
        /// </summary>
        private Rect _rcnormal;
        public Rect Rcnormal
        {
            get{ return _rcnormal; }
            set
            {
                if(_rcnormal != value)
                {
                    _rcnormal = value;
                    if(_rcnormal != null)
                    {
                        HasRcnormal = true;
                    }
                    else
                    {
                        HasRcnormal = false;
                    }
                    OnPropertyChanged(nameof(Rcnormal));
                }
            }
        }

        /// <summary>
        /// 标识 FunctionBar 依赖属性。
        /// </summary>
        public static readonly DependencyProperty FunctionBarProperty =
            DependencyProperty.Register(nameof(FunctionBar), typeof(WindowFunctionBar), typeof(Window), new PropertyMetadata(default(WindowFunctionBar), OnFunctionBarChanged));

        /// <summary>
        /// 获取或设置FunctionBar的值
        /// </summary>
        public WindowFunctionBar FunctionBar
        {
            get => (WindowFunctionBar)GetValue(FunctionBarProperty);
            set => SetValue(FunctionBarProperty, value);
        }

        private static void OnFunctionBarChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            var oldValue = (WindowFunctionBar)args.OldValue;
            var newValue = (WindowFunctionBar)args.NewValue;
            if (oldValue == newValue)
                return;

            var target = obj as Window;
            target?.OnFunctionBarChanged(oldValue, newValue);
        }

        /// <summary>
        /// FunctionBar 属性更改时调用此方法。
        /// </summary>
        /// <param name="oldValue">FunctionBar 属性的旧值。</param>
        /// <param name="newValue">FunctionBar 属性的新值。</param>
        protected virtual void OnFunctionBarChanged(WindowFunctionBar oldValue, WindowFunctionBar newValue)
        {

        }
    }
}
