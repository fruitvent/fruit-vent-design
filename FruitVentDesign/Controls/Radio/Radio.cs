﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Controls
//  
//  Function： N/A
//  Name： Radio
//  
//  Ver       Time                     Author
//  0.10      2021/6/24 14:56:07      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using FruitVentDesign.Enums;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace FruitVentDesign.Controls
{
    public class Radio : RadioButton
    {
        private static readonly ResourceDictionary resourceDictionary = new ResourceDictionary();//资源字典

        public Radio()
        {
            resourceDictionary.Source = new Uri(
               $"pack://application:,,,/FruitVentDesign;component/Styles/Radio/" +
               $"Radios.xaml");//获取资源字典
        }

        static Radio()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Radio), new FrameworkPropertyMetadata(typeof(Radio)));
        }

        #region type
        private static readonly DependencyProperty TypeProperty = DependencyProperty.Register(
            "Type", typeof(RadioType), typeof(Radio), new PropertyMetadata(RadioType.Default, OnRadioTypeChanged));

        private static void OnRadioTypeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            RadioType oldValue = (RadioType)e.OldValue;
            RadioType newValue = (RadioType)e.NewValue;

            if (oldValue == newValue) return;
            Style style = newValue switch
            {
                RadioType.ButtonDefault => resourceDictionary["RadioStyle.ButtonDefault"] as Style,
                RadioType.ButtonSolid => resourceDictionary["RadioStyle.ButtonSolid"] as Style,
                RadioType.Color => resourceDictionary["RadioStyle.Color"] as Style,
                _ => DefaultStyleKeyProperty.DefaultMetadata.DefaultValue as Style,
            };
            if (style != null)
            {
                d.SetValue(StyleProperty, style);
            }
        }

        public RadioType Type
        {
            get { return (RadioType)GetValue(TypeProperty); }
            set { SetValue(TypeProperty, value); }
        }

        public static RadioType GetType(DependencyObject d)
        {
            return (RadioType)d.GetValue(TypeProperty);
        }

        public static void SetType(DependencyObject obj, RadioType value)
        {
            obj.SetValue(TypeProperty, value);
        }
        #endregion

        private static readonly Brush iconColor = new SolidColorBrush(Color.FromRgb(37, 37, 37));
        private static readonly Brush fontColor = new SolidColorBrush(Color.FromRgb(37, 37, 37));
        private static readonly Brush borderColor = new SolidColorBrush(Color.FromRgb(37, 37, 37));

        #region icon property

        #region iconTemplate property
        /// <summary>
        /// icon template
        /// 图标模板
        /// </summary>
        public static readonly DependencyProperty IconTemplateProperty =
            DependencyProperty.Register("IconTemplate", typeof(ControlTemplate), typeof(Radio), new PropertyMetadata(null));

        public ControlTemplate IconTemplate
        {
            get { return (ControlTemplate)GetValue(IconTemplateProperty); }
            set { SetValue(IconTemplateProperty, value); }
        }
        #endregion

        /// <summary>
        /// icon size
        /// 图标大小
        /// </summary>
        public static readonly DependencyProperty IconSizeProperty =
            DependencyProperty.Register("IconSize", typeof(double), typeof(Radio), new PropertyMetadata(18D));

        public double IconSize
        {
            get { return (double)GetValue(IconSizeProperty); }
            set { SetValue(IconSizeProperty, value); }
        }

        /// <summary>
        /// icon margin
        /// 图标间距
        /// </summary>
        public static readonly DependencyProperty IconMarginProperty =
            DependencyProperty.Register("IconMargin", typeof(Thickness), typeof(Radio), new PropertyMetadata(new Thickness(0)));

        public Thickness IconMargin
        {
            get { return (Thickness)GetValue(IconMarginProperty); }
            set { SetValue(IconMarginProperty, value); }
        }

        /// <summary>
        /// icon position
        /// 图标位置
        /// </summary>
        public static readonly DependencyProperty IconPositionProperty =
           DependencyProperty.Register("IconPosition", typeof(Dock), typeof(Radio), new PropertyMetadata(Dock.Left));

        public Dock IconPosition
        {
            get { return (Dock)GetValue(IconPositionProperty); }
            set { SetValue(IconPositionProperty, value); }
        }

        /// <summary>
        /// icon foreground property
        /// 图标颜色（默认）
        /// </summary>
        public static readonly DependencyProperty IconForegroundProperty =
            DependencyProperty.Register("IconForeground", typeof(Brush), typeof(Radio), new PropertyMetadata(iconColor));

        public Brush IconForeground
        {
            get { return (Brush)GetValue(IconForegroundProperty); }
            set { SetValue(IconForegroundProperty, value); }
        }

        /// <summary>
        /// icon foreground when mouse over
        /// 图标颜色（鼠标经过）
        /// </summary>
        public static readonly DependencyProperty MouseOverIconForegroundProperty =
            DependencyProperty.Register("MouseOverIconForeground", typeof(Brush), typeof(Radio), new PropertyMetadata(iconColor));

        public Brush MouseOverIconForeground
        {
            get { return (Brush)GetValue(MouseOverIconForegroundProperty); }
            set { SetValue(MouseOverIconForegroundProperty, value); }
        }

        /// <summary>
        /// icon foreground when mouse pressed
        /// 图标颜色（鼠标按下）
        /// </summary>
        public static readonly DependencyProperty PressedIconForegroundProperty =
            DependencyProperty.Register("PressedIconForeground", typeof(Brush), typeof(Radio), new PropertyMetadata(iconColor));

        public Brush PressedIconForeground
        {
            get { return (Brush)GetValue(PressedIconForegroundProperty); }
            set { SetValue(PressedIconForegroundProperty, value); }
        }

        public static readonly DependencyProperty CheckedIconForegroundProperty =
            DependencyProperty.Register("CheckedIconForeground", typeof(Brush), typeof(Radio), new PropertyMetadata(Brushes.White));
        /// <summary>
        /// 选中前景样式（图标）
        /// </summary>
        public Brush CheckedIconForeground
        {
            get { return (Brush)GetValue(CheckedIconForegroundProperty); }
            set { SetValue(CheckedIconForegroundProperty, value); }
        }
        #endregion

        #region corner radius
        /// <summary>
        /// corner radius
        /// 按钮圆角大小,左上，右上，右下，左下
        /// </summary>
        public static readonly DependencyProperty CornerRadiusProperty =
            DependencyProperty.Register("CornerRadius", typeof(CornerRadius), typeof(Radio), new PropertyMetadata(new CornerRadius(3)));

        public CornerRadius CornerRadius
        {
            get { return (CornerRadius)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }
        #endregion

        #region content decorations
        /// <summary>
        /// content decorations
        /// 文字修饰（下划线，中划线， 基线， 上划线，空心字、立体字、划线字、阴影字、加粗、倾斜等）
        /// </summary>
        public static readonly DependencyProperty ContentDecorationsProperty = DependencyProperty.Register(
            "ContentDecorations", typeof(TextDecorationCollection), typeof(Radio), new PropertyMetadata(null));

        public TextDecorationCollection ContentDecorations
        {
            get { return (TextDecorationCollection)GetValue(ContentDecorationsProperty); }
            set { SetValue(ContentDecorationsProperty, value); }
        }
        #endregion

        #region foreground
        /// <summary>
        /// foreground when mouse over
        /// 文本内容字体颜色（鼠标经过）
        /// </summary>
        public static readonly DependencyProperty MouseOverForegroundProperty =
           DependencyProperty.Register("MouseOverForeground", typeof(Brush), typeof(Radio), new PropertyMetadata(fontColor));

        public Brush MouseOverForeground
        {
            get { return (Brush)GetValue(MouseOverForegroundProperty); }
            set { SetValue(MouseOverForegroundProperty, value); }
        }

        /// <summary>
        /// foreground when mouse pressed
        /// 文本内容字体颜色（鼠标按下）
        /// </summary>
        public static readonly DependencyProperty PressedForegroundProperty =
            DependencyProperty.Register("PressedForeground", typeof(Brush), typeof(Radio), new PropertyMetadata(fontColor));

        public Brush PressedForeground
        {
            get { return (Brush)GetValue(PressedForegroundProperty); }
            set { SetValue(PressedForegroundProperty, value); }
        }

        public static readonly DependencyProperty CheckedForegroundProperty =
           DependencyProperty.Register("CheckedForeground", typeof(Brush), typeof(Radio), new PropertyMetadata(Brushes.White));
        /// <summary>
        /// 选中前景样式（文字）
        /// </summary>
        public Brush CheckedForeground
        {
            get { return (Brush)GetValue(CheckedForegroundProperty); }
            set { SetValue(CheckedForegroundProperty, value); }
        }
        #endregion

        #region background
        /// <summary>
        /// background when mouse over
        /// 按钮颜色（鼠标经过）
        /// </summary>
        public static readonly DependencyProperty MouseOverBackgroundProperty =
            DependencyProperty.Register("MouseOverBackground", typeof(Brush), typeof(Radio), new PropertyMetadata(Brushes.White));

        public Brush MouseOverBackground
        {
            get { return (Brush)GetValue(MouseOverBackgroundProperty); }
            set { SetValue(MouseOverBackgroundProperty, value); }
        }

        /// <summary>
        /// background when mouse pressed
        /// 按钮颜色（鼠标按下）
        /// </summary>
        public static readonly DependencyProperty PressedBackgroundProperty =
            DependencyProperty.Register("PressedBackground", typeof(Brush), typeof(Radio), new PropertyMetadata(Brushes.White));

        public Brush PressedBackground
        {
            get { return (Brush)GetValue(PressedBackgroundProperty); }
            set { SetValue(PressedBackgroundProperty, value); }
        }

        public static readonly DependencyProperty CheckedBackgroundProperty =
            DependencyProperty.Register("CheckedBackground", typeof(Brush), typeof(Radio), new PropertyMetadata(Brushes.DarkBlue));
        /// <summary>
        /// 选中背景样式
        /// </summary>
        public Brush CheckedBackground
        {
            get { return (Brush)GetValue(CheckedBackgroundProperty); }
            set { SetValue(CheckedBackgroundProperty, value); }
        }
        #endregion

        #region border brush
        /// <summary>
        /// border brush when mouse over
        /// 边框颜色（鼠标经过）
        /// </summary>
        public static readonly DependencyProperty MouseOverBorderBrushProperty =
          DependencyProperty.Register("MouseOverBorderBrush", typeof(Brush), typeof(Radio), new PropertyMetadata(borderColor));

        public Brush MouseOverBorderBrush
        {
            get { return (Brush)GetValue(MouseOverBorderBrushProperty); }
            set { SetValue(MouseOverBorderBrushProperty, value); }
        }

        /// <summary>
        /// border brush when mouse pressed
        /// 边框颜色（鼠标按下）
        /// </summary>
        public static readonly DependencyProperty PressedBorderBrushProperty =
            DependencyProperty.Register("PressedBorderBrush", typeof(Brush), typeof(Radio), new PropertyMetadata(borderColor));

        public Brush PressedBorderBrush
        {
            get { return (Brush)GetValue(PressedBorderBrushProperty); }
            set { SetValue(PressedBorderBrushProperty, value); }
        }

        public static readonly DependencyProperty CheckedBorderBrushProperty =
           DependencyProperty.Register("CheckedBorderBrush", typeof(Brush), typeof(Radio), new PropertyMetadata(Brushes.DarkBlue));
        /// <summary>
        /// 鼠标选中边框色
        /// </summary>
        public Brush CheckedBorderBrush
        {
            get { return (Brush)GetValue(CheckedBorderBrushProperty); }
            set { SetValue(CheckedBorderBrushProperty, value); }
        }
        #endregion
    }
}
