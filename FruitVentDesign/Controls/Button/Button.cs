﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Controls
//  
//  Function： N/A
//  Name： Button
//  
//  Ver       Time                     Author
//  0.10      2021/6/16 08:26:34      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using FruitVentDesign.Enums;
using FruitVentDesign.Primitives;
using System;
using System.Diagnostics;
using System.Windows;

namespace FruitVentDesign.Controls
{
    public class Button : ButtonExBase
    {
        private static readonly ResourceDictionary resourceDictionary = new ResourceDictionary();//资源字典

        public Button()
        {
            resourceDictionary.Source = new Uri(
               $"pack://application:,,,/FruitVentDesign;component/Styles/Button/" +
               $"Buttons.xaml");//获取资源字典
        }

        static Button()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(Button), new FrameworkPropertyMetadata(typeof(Button)));
        }

        #region type
        private static readonly DependencyProperty TypeProperty = DependencyProperty.Register(
            "Type", typeof(ButtonType), typeof(Button),new PropertyMetadata(ButtonType.Default, OnButtonTypeChanged));

        private static void OnButtonTypeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ButtonType oldValue = (ButtonType)e.OldValue;
            ButtonType newValue = (ButtonType)e.NewValue;

            if (oldValue == newValue) return;

            Style style = null;

            switch (newValue)
            {
                case ButtonType.Primary:
                    style = resourceDictionary["ButtonStyle.Primary"] as Style;
                    break;
                case ButtonType.Primary_Success:
                    style = resourceDictionary["ButtonStyle.Primary.Success"] as Style;
                    break;
                case ButtonType.Primary_Danger:
                    style = resourceDictionary["ButtonStyle.Primary.Danger"] as Style;
                    break;
                case ButtonType.Link:
                    style = resourceDictionary["ButtonStyle.Link.Defalut"] as Style;
                    break;
                case ButtonType.Link_Danger:
                    style = resourceDictionary["ButtonStyle.Link.Danger"] as Style;
                    break;
                case ButtonType.Text:
                    style = resourceDictionary["ButtonStyle.Text"] as Style;
                    break;
                default:
                    style = DefaultStyleKeyProperty.DefaultMetadata.DefaultValue as Style;
                    break;
            }
            if (style != null)
            {
                d.SetValue(StyleProperty, style);
            }
        }

        public ButtonType Type
        {
            get { return (ButtonType)GetValue(TypeProperty); }
            set { SetValue(TypeProperty, value); }
        }

        public static ButtonType GetType(DependencyObject d)
        {
            return (ButtonType)d.GetValue(TypeProperty);
        }

        public static void SetType(DependencyObject obj, ButtonType value)
        {
            obj.SetValue(TypeProperty, value);
        }
        #endregion
    }
}
