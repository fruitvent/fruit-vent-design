﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Controls
//  
//  Function： N/A
//  Name： FilterHeader
//  
//  Ver       Time                     Author
//  0.10      2021/6/27 22:04:29      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using FruitVentDesign.Commands;
using FruitVentDesign.Models;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace FruitVentDesign.Controls
{
    internal class FilterHeader : ContentControl
    {
        static FilterHeader()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(FilterHeader), new FrameworkPropertyMetadata(typeof(FilterHeader)));
        }

        #region AllowClearProperty 清除输入框Text值按钮启用
        /// <summary>
        /// 清除输入框Text值按钮显示与否
        /// </summary>
        public static readonly DependencyProperty AllowFilterProperty = DependencyProperty.RegisterAttached(
            "AllowFilter", typeof(bool), typeof(FilterHeader), new FrameworkPropertyMetadata(false, AllowFilterChanged));

        public bool AllowFilter
        {
            get { return (bool)GetValue(AllowFilterProperty); }
            set { SetValue(AllowFilterProperty, value); }
        }

        /// <summary>
        /// Gets the brush used to draw the mouse over brush.
        /// </summary>
        public static bool GetAllowFilter(DependencyObject d)
        {
            return (bool)d.GetValue(AllowFilterProperty);
        }

        public static void SetAllowFilter(DependencyObject obj, bool value)
        {
            obj.SetValue(AllowFilterProperty, value);
        }

        private static void AllowFilterChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue != e.NewValue && d is Button button)
            {
                button.CommandBindings.Add(RoutedUICommands.DoFilterCommandBinding);
            }
        }
        #endregion

        #region dependency proeprties
        /// <summary>
        /// title template
        /// </summary>
        public static readonly DependencyProperty TitleTemplateProperty = DependencyProperty.Register(
            "TitleTemplate",
            typeof(DataTemplate),
            typeof(FilterHeader)
        );

        public DataTemplate TitleTemplate
        {
            get { return (DataTemplate)GetValue(TitleTemplateProperty); }
            set { SetValue(TitleTemplateProperty, value); }
        }

        /// <summary>
        /// items used to do filter
        /// </summary>
        public static readonly DependencyProperty FilterItemsProperty = DependencyProperty.Register(
            "FilterItems",
            typeof(IEnumerable<FilterItem>),
            typeof(FilterHeader),
            new PropertyMetadata(null, FilterItemsPropertyChanged)
            );

        private static void FilterItemsPropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            // Trace.WriteLine("filter items property changed");
            if (o is FilterHeader header)
            {
                header.UpdateHasSelectedFilterItems();
            }
        }

        public IEnumerable<FilterItem> FilterItems
        {
            get { return (IEnumerable<FilterItem>)GetValue(FilterItemsProperty); }
            set { SetValue(FilterItemsProperty, value); }
        }

        /// <summary>
        /// title template
        /// </summary>
        public static readonly DependencyProperty HasSelectedFilterItemsProperty = DependencyProperty.Register(
            "HasSelectedFilterItems",
            typeof(bool),
            typeof(FilterHeader),
            new PropertyMetadata(false)
        );

        public bool HasSelectedFilterItems
        {
            get { return (bool)GetValue(HasSelectedFilterItemsProperty); }
            set { SetValue(HasSelectedFilterItemsProperty, value); }
        }

        #endregion

        public static readonly DependencyProperty TitleProperty = DependencyProperty.Register(
            "Title",
            typeof(object),
            typeof(FilterHeader)
        );

        public object Title
        {
            get { return (object)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        internal void UpdateHasSelectedFilterItems()
        {
            // Trace.WriteLine("update has selected filter items");
            bool current;
            var filterItems = FilterItems;
            if (filterItems == null)
            {
                current = false;
            }
            else
            {
                current = false;
                foreach (var item in filterItems)
                {
                    if (item.IsSelected)
                    {
                        current = true;
                        break;
                    }
                }
            }

            if (HasSelectedFilterItems != current)
            {
                HasSelectedFilterItems = current;
            }
        }
    }
}
