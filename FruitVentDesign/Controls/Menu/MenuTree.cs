﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Controls
//  
//  Function： N/A
//  Name： MenuTree
//  
//  Ver       Time                     Author
//  0.10      2021/6/28 20:32:27      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace FruitVentDesign.Controls
{
    public class MenuTree : ItemsControl
    {
        static MenuTree()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(MenuTree), new FrameworkPropertyMetadata(typeof(MenuTree)));
        }

        //protected override void OnItemTemplateChanged(DataTemplate oldItemTemplate, DataTemplate newItemTemplate)
        //{
        //    base.OnItemTemplateChanged(oldItemTemplate, newItemTemplate);
        //    Trace.WriteLine(newItemTemplate.DataType);

        //    if (newItemTemplate is HierarchicalDataTemplate hierarchicalDataTemplate)
        //    {
        //        Trace.WriteLine(hierarchicalDataTemplate.ItemsSource.GetType().ToString());
        //    }
        //}

        //protected override void OnItemsSourceChanged(IEnumerable oldValue, IEnumerable newValue)
        //{
        //    base.OnItemsSourceChanged(oldValue, newValue);
        //    if (oldValue == newValue) return;

        //    if(newValue != null)
        //    {
        //        foreach(var item in newValue)
        //        {
        //            TextBlock textBlock = new TextBlock() { Text = "哈哈" };
        //            AddChild(textBlock);
        //        }
        //    }
        //}
    }
}
