﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace FruitVentDesign.Controls
{
    /// <summary>
    /// Menu.xaml 的交互逻辑
    /// </summary>
    public partial class Menu : UserControl
    {
        private readonly DoubleAnimation _menuAnimation;
        private readonly Storyboard _menuStoryboard;
        private static readonly ResourceDictionary resourceDictionary = new ResourceDictionary();//资源字典

        public Menu()
        {
            InitializeComponent();
            Unloaded += Menu_Unloaded;
            //抽屉式菜单动画初始化
            _menuAnimation = new DoubleAnimation
            {
                Duration = new Duration(TimeSpan.FromSeconds(0.2))
            };

            _menuStoryboard = new Storyboard();
            _menuStoryboard.Children.Add(_menuAnimation);
            Storyboard.SetTarget(_menuAnimation, border);
            Storyboard.SetTargetProperty(_menuAnimation, new PropertyPath(Border.WidthProperty));
            //获取资源字典
            resourceDictionary.Source = new Uri(
               $"pack://application:,,,/FruitVentDesign;component/Styles/Menu/" +
               $"Menus.xaml");

            drawerMenuBtn.IconTemplate = resourceDictionary["MenuStyle.Fold"] as ControlTemplate;
            drawerMenuBtn.ToolTip = "收起";
        }

        private void Menu_Unloaded(object sender, RoutedEventArgs e)
        {
            _menuStoryboard.Remove();
        }

        #region IsFold 是否折叠菜单
        /// <summary>
        /// 是否折叠菜单
        /// </summary>
        public static readonly DependencyProperty IsFoldProperty = DependencyProperty.Register(
            "IsFold", typeof(bool),typeof(Menu),
            new FrameworkPropertyMetadata(false, 
                FrameworkPropertyMetadataOptions.Journal | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, 
                new PropertyChangedCallback(OnIsFoldChanged))
            );

        private static void OnIsFoldChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if(d is Menu self)
            {
                if (e.OldValue == e.NewValue) return;
                self.OnIsFoldChanged(e.OldValue,e.NewValue);
            }
        }

        private void OnIsFoldChanged(object oldPar, object newPar)
        {
            if ((bool)newPar)
            {
                _menuAnimation.From = 200;
                _menuAnimation.To = 55;
                _menuStoryboard.Begin(this);
                drawerMenuBtn.IconTemplate = resourceDictionary["MenuStyle.Unfold"] as ControlTemplate;
                _menuStoryboard.Stop();
            }
            else
            {
                _menuAnimation.From = 55;
                _menuAnimation.To = 200;
                _menuStoryboard.Begin(this);
                drawerMenuBtn.IconTemplate = resourceDictionary["MenuStyle.Fold"] as ControlTemplate;
                drawerMenuBtn.ToolTip = "收起";
                _menuStoryboard.Stop();
            }
        }

        public bool IsFold
        {
            get { return (bool)GetValue(IsFoldProperty); }
            set { SetValue(IsFoldProperty, value); }
        }
        #endregion

        #region MenusSource 菜单数据源
        /// <summary>
        /// 菜单数据源
        /// </summary>
        public static readonly DependencyProperty MenusSourceProperty = DependencyProperty.Register(
            "MenusSource",typeof(object),typeof(Menu),
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.Journal | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault)
            );

        public object MenusSource
        {
            get { return (object)GetValue(MenusSourceProperty); }
            set
            {
                SetValue(MenusSourceProperty, value);
            }
        }
        #endregion

        #region Menu tree item template
        /// <summary>
        /// Menu tree item template
        /// </summary>
        public static readonly DependencyProperty ItemTemplateProperty = DependencyProperty.Register(
            "ItemTemplate",
            typeof(DataTemplate),
            typeof(Menu)
        );

        public DataTemplate ItemTemplate
        {
            get { return (DataTemplate)GetValue(ItemTemplateProperty); }
            set { SetValue(ItemTemplateProperty, value); }
        }
        #endregion

        #region Drawer Menu 抽屉式菜单
        /// <summary>
        /// 收起展开
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DrawerMenuBtn_Click(object sender, RoutedEventArgs e)
        {
            IsFold = !IsFold;
        }
        #endregion
    }
}
