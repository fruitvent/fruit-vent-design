﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Controls
//  
//  Function： N/A
//  Name： MenuItem
//  
//  Ver       Time                     Author
//  0.10      2021/6/29 15:39:59      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using FruitVentDesign.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace FruitVentDesign.Controls
{
    public class MenuItem : Radio
    {
        static MenuItem()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(MenuItem), new FrameworkPropertyMetadata(typeof(MenuItem)));
        }

        public MenuItem()
        {
            Click += ButtonMenu_Click;
        }

        protected virtual void ButtonMenu_Click(object sender, RoutedEventArgs e){ }

        /// <summary>
        /// 查找某种类型的子控件，并返回一个List集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="typename"></param>
        /// <returns></returns>
        public List<T> GetChildObjects<T>(DependencyObject obj, Type typename) where T : FrameworkElement
        {
            List<T> childList = new List<T>();

            for (int i = 0; i <= VisualTreeHelper.GetChildrenCount(obj) - 1; i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);

                if (child is T t && (t.GetType() == typename))
                {
                    childList.Add(t);
                }
                childList.AddRange(GetChildObjects<T>(child, typename));
            }
            return childList;
        }

        //#region IsSelected 
        ///// <summary>
        ///// is selected
        ///// </summary>
        //public static readonly DependencyProperty IsSelectedProperty = DependencyProperty.Register(
        //    "IsSelected", typeof(bool), typeof(Menu),
        //    new FrameworkPropertyMetadata(false,
        //        FrameworkPropertyMetadataOptions.Journal | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault)
        //    );

        //public bool IsSelected
        //{
        //    get { return (bool)GetValue(IsSelectedProperty); }
        //    set { SetValue(IsSelectedProperty, value); }
        //}
        //#endregion
    }
}
