﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Controls
//  
//  Function： N/A
//  Name： SubMenu
//  
//  Ver       Time                     Author
//  0.10      2021/6/29 15:40:48      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace FruitVentDesign.Controls
{
    public class SubMenu : ToggleButtonEx
    {
        #region 
        public static readonly DependencyProperty IsExpandedProperty = DependencyProperty.Register(
         "IsExpanded",
         typeof(bool),
         typeof(SubMenu),
         new FrameworkPropertyMetadata(false)
         );

        public bool IsExpanded
        {
            get { return (bool)GetValue(IsExpandedProperty); }
            set { SetValue(IsExpandedProperty, value); }
        }
        #endregion

        public SubMenu()
        {
            Click += ButtonMenu_Click;
        }

        private void ButtonMenu_Click(object sender, RoutedEventArgs e)
        {
            ToggleButton tg = (ToggleButton)sender;

            Window win = Window.GetWindow(this);
            List<SubMenu> list = GetChildObjects<SubMenu>(win, typeof(SubMenu));
            foreach (var item in list.Where(x => x != this))
            {
                if (item.Visibility != Visibility.Visible) { continue; }

                if (object.ReferenceEquals(item, tg))
                {
                    item.IsChecked = true;
                }
                else
                {
                    item.IsChecked = false;
                }
            }
        }

        /// <summary>
        /// 查找某种类型的子控件，并返回一个List集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="typename"></param>
        /// <returns></returns>
        public List<T> GetChildObjects<T>(DependencyObject obj, Type typename) where T : FrameworkElement
        {
            List<T> childList = new List<T>();

            for (int i = 0; i <= VisualTreeHelper.GetChildrenCount(obj) - 1; i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);

                if (child is T && (((T)child).GetType() == typename))
                {
                    childList.Add((T)child);
                }
                childList.AddRange(GetChildObjects<T>(child, typename));
            }
            return childList;
        }

        //static SubMenu()
        //{
        //    DefaultStyleKeyProperty.OverrideMetadata(typeof(SubMenu), new FrameworkPropertyMetadata(typeof(SubMenu)));
        //}

        //#region IsExpanded 
        ///// <summary>
        ///// is expanded
        ///// </summary>
        //public static readonly DependencyProperty IsExpandedProperty = DependencyProperty.Register(
        //    "IsExpanded", typeof(bool), typeof(Menu),
        //    new FrameworkPropertyMetadata(false,
        //        FrameworkPropertyMetadataOptions.Journal | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault)
        //    );

        //public bool IsExpanded
        //{
        //    get { return (bool)GetValue(IsExpandedProperty); }
        //    set { SetValue(IsExpandedProperty, value); }
        //}
        //#endregion
    }
}
