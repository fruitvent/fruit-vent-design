﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Controls
//  
//  Function： N/A
//  Name： FormFunctionBar
//  
//  Ver       Time                     Author
//  0.10      2021/6/21 11:27:59      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

namespace FruitVentDesign.Controls
{
    public class FormFunctionBar : FunctionBar
    {
        public FormFunctionBar()
        {
            DefaultStyleKey = typeof(FormFunctionBar);
        }
    }
}
