﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Controls
//  
//  Function： N/A
//  Name： Form
//  
//  Ver       Time                     Author
//  0.10      2021/6/20 23:41:30      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Markup;

namespace FruitVentDesign.Controls
{
    [StyleTypedProperty(Property = "ItemContainerStyle", StyleTargetType = typeof(FormItem))]
    public partial class Form : HeaderedItemsControl
    {
        private DataTemplate _labelMemberTemplate;

        public Form()
        {
            DefaultStyleKey = typeof(Form);
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            var obj = (Template.FindName("FunctionBar", this) as ContentPresenter).Content as DependencyObject;

            if (obj == null) return;

            foreach (object child in LogicalTreeHelper.GetChildren(obj))
            {
                if (child is Button button && button.Tag != null && button.Tag.ToString() == "submit")
                {
                    button.Click += SubmitButton_Click;
                }
            }
        }

        /// <summary>
        /// 提交按钮点击
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SubmitButton_Click(object sender, RoutedEventArgs e)
        {
            OnFinish();
        }

        private DataTemplate LabelMemberTemplate
        {
            get
            {
                if (_labelMemberTemplate == null)
                {
                    _labelMemberTemplate = (DataTemplate)XamlReader.Parse(@"
                    <DataTemplate xmlns=""http://schemas.microsoft.com/winfx/2006/xaml/presentation""
                                xmlns:x=""http://schemas.microsoft.com/winfx/2006/xaml"">
                    		<TextBlock Text=""{Binding " + LabelMemberPath + @"}"" VerticalAlignment=""Center""/>
                    </DataTemplate>");
                }

                return _labelMemberTemplate;
            }
        }

        /// <summary>
        /// FunctionBar 属性更改时调用此方法。
        /// </summary>
        /// <param name="oldValue">FunctionBar 属性的旧值。</param>
        /// <param name="newValue">FunctionBar 属性的新值。</param>
        protected virtual void OnFunctionBarChanged(FormFunctionBar oldValue, FormFunctionBar newValue)
        {
            
        }

        /// <summary>
        /// LabelMemberPath 属性更改时调用此方法。
        /// </summary>
        /// <param name="oldValue">LabelMemberPath 属性的旧值。</param>
        /// <param name="newValue">LabelMemberPath 属性的新值。</param>
        protected virtual void OnLabelMemberPathChanged(string oldValue, string newValue)
        {
            // refresh the label member template.
            _labelMemberTemplate = null;
#pragma warning disable IDE0059 // 从不使用分配给符号的值
            var newTemplate = LabelMemberPath;
#pragma warning restore IDE0059 // 从不使用分配给符号的值

            int count = Items.Count;
            for (int i = 0; i < count; i++)
            {
                if (ItemContainerGenerator.ContainerFromIndex(i) is FormItem formItem)
                    PrepareFormItem(formItem, Items[i]);
            }
        }

        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            bool isItemItsOwnContainer = false;
            if (item is FrameworkElement element)
                isItemItsOwnContainer = GetIsItemItsOwnContainer(element);

            return item is FormItem || isItemItsOwnContainer;
        }

        protected override DependencyObject GetContainerForItemOverride()
        {
            var item = new FormItem();
            return item;
        }

        protected override void PrepareContainerForItemOverride(DependencyObject element, object item)
        {
            base.PrepareContainerForItemOverride(element, item);

            if (element is FormItem formItem && item is FormItem == false)
            {
                if (item is FrameworkElement content)
                    PrepareFormFrameworkElement(formItem, content);
                else
                    PrepareFormItem(formItem, item);
            }
        }

        protected override bool ShouldApplyItemContainerStyle(DependencyObject container, object item)
        {
            return container is FormItem;
        }

        private static void OnFunctionBarChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            var oldValue = (FormFunctionBar)args.OldValue;
            var newValue = (FormFunctionBar)args.NewValue;
            if (oldValue == newValue)
                return;

            var target = obj as Form;
            target?.OnFunctionBarChanged(oldValue, newValue);
        }

        private static void OnLabelMemberPathChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            var oldValue = (string)args.OldValue;
            var newValue = (string)args.NewValue;
            if (oldValue == newValue)
                return;

            var target = obj as Form;
            target?.OnLabelMemberPathChanged(oldValue, newValue);
        }

        private static void OnFormPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            if (args.OldValue == args.NewValue)
                return;

            if (obj is FrameworkElement content && content.Parent is Form form)
            {
                if (form.ItemContainerGenerator.ContainerFromItem(content) is FormItem formItem)
                    form.PrepareFormFrameworkElement(formItem, content);
            }
        }

        private void PrepareFormFrameworkElement(FormItem formItem, FrameworkElement content)
        {
            formItem.Label = GetLabel(content);
            formItem.Description = GetDescription(content);
            formItem.IsRequired = GetIsRequired(content);
            formItem.LabelPosition = GetLabelPosition(content);
            formItem.LabelWidth = GetLabelWidth(content);
            formItem.Width = GetFormItemWidth(content);
            formItem.ClearValue(DataContextProperty);
            Style style = GetContainerStyle(content);
            if (style != null)
                formItem.Style = style;
            else if (ItemContainerStyle != null)
                formItem.Style = ItemContainerStyle;
            else
                formItem.ClearValue(FrameworkElement.StyleProperty);

            DataTemplate labelTemplate = GetLabelTemplate(content);
            if (labelTemplate != null)
                formItem.LabelTemplate = labelTemplate;

            var binding = new Binding(nameof(Visibility))
            {
                Source = content,
                Mode = BindingMode.OneWay
            };
            formItem.SetBinding(VisibilityProperty, binding);
        }

        private void PrepareFormItem(FormItem formItem, object item)
        {
            if (formItem == item)
                return;

            if (item is FrameworkElement)
                return;

            formItem.LabelTemplate = LabelMemberTemplate;
            formItem.Label = item;
        }

        private void GetErrors(out List<string> messages, DependencyObject obj)
        {
            messages = new List<string>();
            foreach (object child in LogicalTreeHelper.GetChildren(obj))
            {
                if (child is Input input)
                {
                    if (Validation.GetHasError(input))
                    {
                        foreach (ValidationError error in Validation.GetErrors(input))
                        {
                            messages.Add(error.ErrorContent.ToString());
                        }
                    }
                }
                else if (child is Select select)
                {
                    if (Validation.GetHasError(select))
                    {
                        foreach (ValidationError error in Validation.GetErrors(select))
                        {
                            messages.Add(error.ErrorContent.ToString());
                        }
                    }
                }
                else if (child is DatePicker datePicker)
                {
                    if (Validation.GetHasError(datePicker))
                    {
                        foreach (ValidationError error in Validation.GetErrors(datePicker))
                        {
                            messages.Add(error.ErrorContent.ToString());
                        }
                    }
                }
                else if (child is DateTimePicker dateTimePicker)
                {
                    if (Validation.GetHasError(dateTimePicker))
                    {
                        foreach (ValidationError error in Validation.GetErrors(dateTimePicker))
                        {
                            messages.Add(error.ErrorContent.ToString());
                        }
                    }
                }
                else if (child is PasswordInput passwordInput)
                {
                    if (Validation.GetHasError(passwordInput))
                    {
                        foreach (ValidationError error in Validation.GetErrors(passwordInput))
                        {
                            messages.Add(error.ErrorContent.ToString());
                        }
                    }
                }
                else if (child is MultiSelect multiSelect)
                {
                    if (Validation.GetHasError(multiSelect))
                    {
                        foreach (ValidationError error in Validation.GetErrors(multiSelect))
                        {
                            messages.Add(error.ErrorContent.ToString());
                        }
                    }
                }
                else
                {
                    continue;
                }
            }
        }

        #region validate fields command
        public event EventHandler Finish;

        public static readonly DependencyProperty FinishCommandProperty = DependencyProperty.Register(
            "FinishCommand",
            typeof(ICommand),
            typeof(Form)
            );

        public ICommand FinishCommand
        {
            get { return (ICommand)GetValue(FinishCommandProperty); }
            set { SetValue(FinishCommandProperty, value); }
        }

        /// <summary>
        /// fire event and command
        /// </summary>
        protected void OnFinish()
        {
            Finish?.Invoke(this, EventArgs.Empty);

            List<string> errormessages;
            GetErrors(out errormessages, this);
            if (FinishCommand != null && FinishCommand.CanExecute(errormessages))
            {
                FinishCommand.Execute(errormessages);
            }
        }
        #endregion
    }
}
