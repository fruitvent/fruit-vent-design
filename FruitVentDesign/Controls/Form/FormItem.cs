﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Controls
//  
//  Function： N/A
//  Name： FormItem
//  
//  Ver       Time                     Author
//  0.10      2021/6/20 23:42:00      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using FruitVentDesign.Enums;
using System;
using System.Windows;
using System.Windows.Controls;

namespace FruitVentDesign.Controls
{
    public class FormItem : ContentControl
    {
        public FormItem()
        {
            DefaultStyleKey = typeof(FormItem);
        }

        #region LabelProperty: FormItem的头部Label
        /// <summary>
        /// FormItem的头部Label
        /// </summary>
        public static readonly DependencyProperty LabelProperty = DependencyProperty.Register(
            "Label", typeof(object), typeof(FormItem), new PropertyMetadata(default, OnLabelChanged));

        public object Label
        {
            get { return (object)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        /// <summary>
        /// Label 属性更改时调用此方法。
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="args"></param>
        private static void OnLabelChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            var oldValue = (object)args.OldValue;
            var newValue = (object)args.NewValue;
            if (oldValue == newValue)
                return;

            var target = obj as FormItem;
            target?.OnLabelChanged(oldValue, newValue);
        }

        /// <summary>
        /// Label 属性更改时调用此方法。
        /// </summary>
        /// <param name="oldValue">Label 属性的旧值。</param>
        /// <param name="newValue">Label 属性的新值。</param>
        protected virtual void OnLabelChanged(object oldValue, object newValue)
        {
        }
        #endregion

        #region LabelWidthProperty: FormItem的头部Label宽度
        /// <summary>
        /// FormItem的头部Label宽度
        /// </summary>
        public static readonly DependencyProperty LabelWidthProperty = DependencyProperty.Register(
            "LabelWidth", typeof(double), typeof(FormItem), new PropertyMetadata(Double.NaN));

        public double LabelWidth
        {
            get { return (double)GetValue(LabelWidthProperty); }
            set { SetValue(LabelWidthProperty, value); }
        }
        #endregion

        #region LabelPositionProperty: FormItem的头部Label位置
        /// <summary>
        /// FormItem的头部Label位置
        /// </summary>
        public static readonly DependencyProperty LabelPositionProperty = DependencyProperty.Register(
            "LabelPosition", typeof(FormItemLabelPosition), typeof(FormItem), new PropertyMetadata(FormItemLabelPosition.Right));

        public FormItemLabelPosition LabelPosition
        {
            get { return (FormItemLabelPosition)GetValue(LabelPositionProperty); }
            set { SetValue(LabelPositionProperty, value); }
        }
        #endregion

        #region LabelTemplateProperty: FormItem的Label模板
        /// <summary>
        /// TextBox的头部Label模板
        /// </summary>
        public static readonly DependencyProperty LabelTemplateProperty = DependencyProperty.Register(
            "LabelTemplate", typeof(DataTemplate), typeof(FormItem), new FrameworkPropertyMetadata(default(DataTemplate), OnLabelTemplateChanged));

        public DataTemplate LabelTemplate
        {
            get { return (DataTemplate)GetValue(LabelTemplateProperty); }
            set { SetValue(LabelTemplateProperty, value); }
        }

        private static void OnLabelTemplateChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            var oldValue = (DataTemplate)args.OldValue;
            var newValue = (DataTemplate)args.NewValue;
            if (oldValue == newValue)
                return;

            var target = obj as FormItem;
            target?.OnLabelTemplateChanged(oldValue, newValue);
        }

        /// <summary>
        /// LabelTemplate 属性更改时调用此方法。
        /// </summary>
        /// <param name="oldValue">LabelTemplate 属性的旧值。</param>
        /// <param name="newValue">LabelTemplate 属性的新值。</param>
        protected virtual void OnLabelTemplateChanged(DataTemplate oldValue, DataTemplate newValue)
        {
        }
        #endregion

        #region IsRequiredProperty: 设置必填字段
        /// <summary>
        /// 设置必填字段
        /// </summary>
        public static readonly DependencyProperty IsRequiredProperty = DependencyProperty.Register(
            "IsRequired", typeof(bool), typeof(FormItem), new FrameworkPropertyMetadata(false));

        public bool IsRequired
        {
            get { return (bool)GetValue(IsRequiredProperty); }
            set { SetValue(IsRequiredProperty, value); }
        }
        #endregion

        #region DescriptionProperty: FormItem备注信息
        /// <summary>
        /// FormItem备注信息
        /// </summary>
        public static readonly DependencyProperty DescriptionProperty = DependencyProperty.Register(
            "Description", typeof(object), typeof(FormItem), new PropertyMetadata(null));

        public object Description
        {
            get { return (object)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }
        #endregion

        #region ErrorMessage 验证未通过消息
        /// <summary>
        /// 验证未通过消息
        /// </summary>
        public static readonly DependencyProperty ErrorMessageProperty = DependencyProperty.Register(
            "ErrorMessage", typeof(object), typeof(FormItem), new FrameworkPropertyMetadata(null,
             FrameworkPropertyMetadataOptions.Journal | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public object ErrorMessage
        {
            get { return (object)GetValue(ErrorMessageProperty); }
            set { SetValue(ErrorMessageProperty, value); }
        }
        #endregion

        #region HasErrorMessage 验证通过与否字段
        /// <summary>
        /// 验证通过与否字段
        /// </summary>
        public static readonly DependencyProperty HasErrorMessageProperty = DependencyProperty.Register(
            "HasErrorMessage", typeof(bool), typeof(FormItem), new FrameworkPropertyMetadata(false));

        public bool HasErrorMessage
        {
            get { return (bool)GetValue(HasErrorMessageProperty); }
            set { SetValue(HasErrorMessageProperty, value); }
        }

        public static void SetHasErrorMessage(DependencyObject element, bool value)
        {
            element.SetValue(HasErrorMessageProperty, value);
        }

        public static bool GetHasErrorMessage(DependencyObject element)
        {
            return (bool)element.GetValue(HasErrorMessageProperty);
        }
        #endregion
    }
}
