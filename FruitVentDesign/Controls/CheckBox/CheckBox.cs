﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Controls
//  
//  Function： N/A
//  Name： CheckBox
//  
//  Ver       Time                     Author
//  0.10      2021/6/23 16:40:14      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using FruitVentDesign.Enums;
using System;
using System.Windows;
using System.Windows.Media;

namespace FruitVentDesign.Controls
{
    public class CheckBox : System.Windows.Controls.CheckBox
    {
        private static readonly ResourceDictionary resourceDictionary = new ResourceDictionary();//资源字典

        static CheckBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CheckBox), new FrameworkPropertyMetadata(typeof(CheckBox)));
        }

        public CheckBox()
        {
            resourceDictionary.Source = new Uri(
               $"pack://application:,,,/FruitVentDesign;component/Styles/CheckBox/" +
               $"CheckBoxs.xaml");//获取资源字典
        }

        #region type
        private static readonly DependencyProperty TypeProperty = DependencyProperty.Register(
            "Type", typeof(CheckBoxType), typeof(CheckBox), new PropertyMetadata(CheckBoxType.Default, OnButtonTypeChanged));

        private static void OnButtonTypeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            CheckBoxType oldValue = (CheckBoxType)e.OldValue;
            CheckBoxType newValue = (CheckBoxType)e.NewValue;

            if (oldValue == newValue) return;
            Style style = newValue switch
            {
                CheckBoxType.Rimless => resourceDictionary["CheckBoxStyle.Rimless"] as Style,
                CheckBoxType.FilledCircle => resourceDictionary["CheckBoxStyle.FilledCircle"] as Style,
                CheckBoxType.Slider => resourceDictionary["CheckBoxStyle.Slider"] as Style,
                _ => DefaultStyleKeyProperty.DefaultMetadata.DefaultValue as Style,
            };
            if (style != null)
            {
                d.SetValue(StyleProperty, style);
            }
        }

        public CheckBoxType Type
        {
            get { return (CheckBoxType)GetValue(TypeProperty); }
            set { SetValue(TypeProperty, value); }
        }

        public static CheckBoxType GetType(DependencyObject d)
        {
            return (CheckBoxType)d.GetValue(TypeProperty);
        }

        public static void SetType(DependencyObject obj, CheckBoxType value)
        {
            obj.SetValue(TypeProperty, value);
        }
        #endregion

        /// <summary>
        /// icon size
        /// 图标大小
        /// </summary>
        public static readonly DependencyProperty IconSizeProperty =
            DependencyProperty.Register("IconSize", typeof(double), typeof(CheckBox), new PropertyMetadata(18D));

        public double IconSize
        {
            get { return (double)GetValue(IconSizeProperty); }
            set { SetValue(IconSizeProperty, value); }
        }

        /// <summary>
        /// icon margin
        /// 图标间距
        /// </summary>
        public static readonly DependencyProperty IconMarginProperty =
            DependencyProperty.Register("IconMargin", typeof(Thickness), typeof(CheckBox), new PropertyMetadata(new Thickness(0)));

        public Thickness IconMargin
        {
            get { return (Thickness)GetValue(IconMarginProperty); }
            set { SetValue(IconMarginProperty, value); }
        }
        /// <summary>
        /// icon foreground property
        /// 图标颜色（默认）
        /// </summary>
        public static readonly DependencyProperty IconForegroundProperty =
            DependencyProperty.Register("IconForeground", typeof(Brush), typeof(CheckBox), new PropertyMetadata(new SolidColorBrush(Color.FromRgb(217, 217, 217))));

        public Brush IconForeground
        {
            get { return (Brush)GetValue(IconForegroundProperty); }
            set { SetValue(IconForegroundProperty, value); }
        }

        /// <summary>
        /// icon foreground when mouse over
        /// 图标颜色（鼠标经过）
        /// </summary>
        public static readonly DependencyProperty MouseOverIconForegroundProperty =
            DependencyProperty.Register("MouseOverIconForeground", typeof(Brush), typeof(CheckBox), new PropertyMetadata(new SolidColorBrush(Color.FromRgb(217, 217, 217))));

        public Brush MouseOverIconForeground
        {
            get { return (Brush)GetValue(MouseOverIconForegroundProperty); }
            set { SetValue(MouseOverIconForegroundProperty, value); }
        }

        /// <summary>
        /// icon foreground when get fouced
        /// 图标颜色（获得焦点）
        /// </summary>
        public static readonly DependencyProperty FocusedIconForegroundProperty =
            DependencyProperty.Register("FocusedIconForeground", typeof(Brush), typeof(CheckBox), new PropertyMetadata(new SolidColorBrush(Color.FromRgb(217, 217, 217))));

        public Brush FocusedIconForeground
        {
            get { return (Brush)GetValue(FocusedIconForegroundProperty); }
            set { SetValue(FocusedIconForegroundProperty, value); }
        }

        /// <summary>
        /// icon foreground when get fouced
        /// 图标颜色（获得焦点）
        /// </summary>
        public static readonly DependencyProperty CheckedIconForegroundProperty =
            DependencyProperty.Register("CheckedIconForeground", typeof(Brush), typeof(CheckBox), new PropertyMetadata(new SolidColorBrush(Color.FromRgb(217, 217, 217))));

        public Brush CheckedIconForeground
        {
            get { return (Brush)GetValue(CheckedIconForegroundProperty); }
            set { SetValue(CheckedIconForegroundProperty, value); }
        }
    }
}
