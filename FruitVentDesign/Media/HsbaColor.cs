﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Media
//  
//  Function： N/A
//  Name： HsbaColor
//  
//  Ver       Time                     Author
//  0.10      2021/11/23 9:48:22                   Benedict Deng
//
//  Description:
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

using System.Windows.Media;

namespace FruitVentDesign.Media
{
    public class HsbaColor
    {
        double h = 0, s = 0, b = 0, a = 0;
        /// <summary>
        /// 0 - 359，360 = 0
        /// </summary>
        public double H { get => h; set => h = value < 0 ? 0 : value >= 360 ? 0 : value; }
        /// <summary>
        /// 0 - 1
        /// </summary>
        public double S { get => s; set => s = value < 0 ? 0 : value > 1 ? 1 : value; }
        /// <summary>
        /// 0 - 1
        /// </summary>
        public double B { get => b; set => b = value < 0 ? 0 : value > 1 ? 1 : value; }
        /// <summary>
        /// 0 - 1
        /// </summary>
        public double A { get => a; set => a = value < 0 ? 0 : value > 1 ? 1 : value; }
        /// <summary>
        /// 亮度 0 - 100
        /// </summary>
        public int Y => RgbaColor.Y;

        public HsbaColor() { H = 0; S = 0; B = 1; A = 1; }
        public HsbaColor(double h, double s, double b, double a = 1) { H = h; S = s; B = b; A = a; }
        public HsbaColor(int r, int g, int b, int a = 255)
        {
            HsbaColor hsba = Utility.RgbaToHsba(new RgbaColor(r, g, b, a));
            H = hsba.H;
            S = hsba.S;
            B = hsba.B;
            A = hsba.A;
        }
        public HsbaColor(Brush brush)
        {
            HsbaColor hsba = Utility.RgbaToHsba(new RgbaColor(brush));
            H = hsba.H;
            S = hsba.S;
            B = hsba.B;
            A = hsba.A;
        }
        public HsbaColor(string hexColor)
        {
            HsbaColor hsba = Utility.RgbaToHsba(new RgbaColor(hexColor));
            H = hsba.H;
            S = hsba.S;
            B = hsba.B;
            A = hsba.A;
        }

        public Color Color => RgbaColor.Color;
        public Color OpaqueColor => RgbaColor.OpaqueColor;
        public SolidColorBrush SolidColorBrush => RgbaColor.SolidColorBrush;
        public SolidColorBrush OpaqueSolidColorBrush => RgbaColor.OpaqueSolidColorBrush;

        public string HexString => Color.ToString();
        public string RgbaString => RgbaColor.RgbaString;

        public RgbaColor RgbaColor => Utility.HsbaToRgba(this);
    }
}
