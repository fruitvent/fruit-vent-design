﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Media
//  
//  Function： N/A
//  Name： ResObj
//  
//  Ver       Time                     Author
//  0.10      2021/11/23 9:46:04                   Benedict Deng
//
//  Description:
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

using System.IO;
using System.Reflection;
using System.Windows.Media;

namespace FruitVentDesign.Media
{
    public class ResObj
    {
        public static Stream Get(Assembly assembly, string path)
        {
            return assembly.GetManifestResourceStream(assembly.GetName().Name + "." + path);
        }

        public static string GetString(Assembly assembly, string path)
        {
            try
            {
                return StreamObj.ToString(Get(assembly, path));
            }
            catch
            {
                return null;
            }
        }

        public static ImageSource GetImageSource(Assembly assembly, string path)
        {
            try
            {
                return StreamObj.ToImageSource(Get(assembly, path));
            }
            catch
            {
                return null;
            }
        }
    }
}
