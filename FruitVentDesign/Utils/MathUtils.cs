﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Utils
//  
//  Function： N/A
//  Name： MathUtils
//  
//  Ver       Time                     Author
//  0.10      2021/6/17 11:59:25      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using System;

namespace FruitVentDesign.Utils
{
    public static class MathUtils
    {
        /// <summary>
        /// caculate top margin in border
        /// </summary>
        /// <param name="angle"></param>
        /// <param name="borderRadius"></param>
        /// <param name="circleRadius"></param>
        /// <returns></returns>
        public static double TopMargin(double angle, double borderRadius, double circleRadius)
        {
            // convert to radians
            double radian = (Math.PI / 180.0) * angle;
            double y = (borderRadius - circleRadius) * Math.Sin(radian);
            y += circleRadius;
            return borderRadius - y;
        }

        /// <summary>
        /// caculate left margin in border
        /// </summary>
        /// <param name="angle"></param>
        /// <param name="borderRadius"></param>
        /// <param name="circleRadius"></param>
        /// <returns></returns>
        public static double LeftMargin(double angle, double borderRadius, double circleRadius)
        {
            // convert to radians
            double radian = (Math.PI / 180.0) * angle;
            double x = (borderRadius - circleRadius) * Math.Cos(radian);
            x -= circleRadius;
            return borderRadius + x;
        }
    }
}
