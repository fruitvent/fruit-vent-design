﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Utils
//  
//  Function： N/A
//  Name： ThemeAssist
//  
//  Ver       Time                     Author
//  0.10      2021/6/16 10:23:17      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using FruitVentDesign.Enums;
using System;
using System.Windows;

namespace FruitVentDesign.Utils
{
    /// <summary>
    /// 主题颜色管理类
    /// </summary>
    public class ThemeAssist
    {
        public static void ChangeTheme(ThemeKind themeName)
        {
            var mergedDictionaries = Application.Current.Resources.MergedDictionaries;

            foreach (var merged in mergedDictionaries)
            {
                if (merged.Source.ToString().Contains(nameof(ThemeKind.Orange))
                    || merged.Source.ToString().Contains(nameof(ThemeKind.SkyBlue))
                    || merged.Source.ToString().Contains(nameof(ThemeKind.Blue)))
                {
                    mergedDictionaries.Remove(merged);

                    break;
                }
            }
            mergedDictionaries.Add(new ResourceDictionary { Source = new Uri($"pack://application:,,,/FruitVentDesign;component/Skins/{themeName}.xaml") });
        }
    }
}
