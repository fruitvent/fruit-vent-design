﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesign.Utils
//  
//  Function： N/A
//  Name： ConverterUtil
//  
//  Ver       Time                     Author
//  0.10      2021/6/16 10:03:48      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using FruitVentDesign.Converters;

namespace FruitVentDesign.Utils
{
    public class ConverterUtil
    {
        public static EmptyObjectToVisibilityConverter EmptyObjectToVisibilityConverter { get; } = new EmptyObjectToVisibilityConverter();
        public static ObjectToIconConverter ObjectToIconConverter { get; } = new ObjectToIconConverter();
        public static IconPositionToContentPositionConverter IconPositionToContentPositionConverter { get; } = new IconPositionToContentPositionConverter();
        public static IconPositionToPaddingConverter IconPositionToPaddingConverter { get; } = new IconPositionToPaddingConverter();
        public static IconPositionToIconMagrinConverter IconPositionToIconMagrinConverter { get; } = new IconPositionToIconMagrinConverter();
        public static BooleanToVisibilityConverter BooleanToVisibilityConverter { get; } = new BooleanToVisibilityConverter();
        public static CornerRadiusToLabelCornerRadiusConverter CornerRadiusToLabelCornerRadiusConverter { get; } = new CornerRadiusToLabelCornerRadiusConverter();
        public static IsEmptyObjectConverter IsEmptyObjectConverter { get; } = new IsEmptyObjectConverter();
        public static FormItemLabelPositionToDockConverter FormItemLabelPositionToDockConverter { get; } = new FormItemLabelPositionToDockConverter();
        public static FormLayoutToLayoutConverter FormLayoutToLayoutConverter { get; } = new FormLayoutToLayoutConverter();
        public static FormLayoutToLeftRightFormItemLayoutConverter FormLayoutToLeftRightFormItemLayoutConverter { get; } = new FormLayoutToLeftRightFormItemLayoutConverter();
        public static FormLayoutToUpDownFormItemLayoutConverter FormLayoutToUpDownFormItemLayoutConverter { get; } = new FormLayoutToUpDownFormItemLayoutConverter();
        public static CalculateDropWidthConverter CalculateDropWidthConverter { get; } = new CalculateDropWidthConverter();
        public static IndentConverter IndentConverter { get; set; } = new IndentConverter();
        public static FilterHeaderHorizontalAlignmentToMagrinConverter FilterHeaderHorizontalAlignmentToMagrinConverter { get; } = new FilterHeaderHorizontalAlignmentToMagrinConverter();
        public static HasChildrenMenuBtnToVisibilityConverter HasChildrenMenuBtnToVisibilityConverter { get; } = new HasChildrenMenuBtnToVisibilityConverter();
        public static NoChildrenMenuBtnToVisibilityConverter NoChildrenMenuBtnToVisibilityConverter { get; } = new NoChildrenMenuBtnToVisibilityConverter();
        public static ThemeKindToBoolConverter ThemeKindToBoolConverter { get; } = new ThemeKindToBoolConverter();
        public static PagesTypeAndTagToBooleanConverter PagesTypeAndTagToBooleanConverter { get; } = new PagesTypeAndTagToBooleanConverter();
        public static IsShowTableSortConverter IsShowTableSortConverter { get; } = new IsShowTableSortConverter();
        public static DatePickerTypeToVisibilityConverter DatePickerTypeToVisibilityConverter { get; } = new DatePickerTypeToVisibilityConverter();
    }
}
