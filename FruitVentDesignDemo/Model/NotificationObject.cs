﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesignDemo.Model
//  
//  Function： N/A
//  Name： NotificationObject
//  
//  Ver       Time                     Author
//  0.10      2021/9/18 9:23:08                   Benedict Deng
//
//  Description:
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace FruitVentDesignDemo.Model
{
    public class NotificationObject : INotifyPropertyChanged, IDataErrorInfo
    {

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// WPF使用不到该属性
        /// </summary>
        public string Error
        {
            get { return null; }
        }

        /// <summary>
        /// 验证
        /// </summary>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public string this[string columnName]
        {
            get
            {
                ValidationContext vc = new ValidationContext(this, null, null);
                vc.MemberName = columnName;
                List<ValidationResult> res = new List<ValidationResult>();
                bool result = Validator.TryValidateProperty(GetType().GetProperty(columnName).GetValue(this, null), vc, res);
                if (res.Count > 0)
                {
                    return string.Join(Environment.NewLine, res.Select(r => r.ErrorMessage).ToArray());
                }
                return string.Empty;
            }
        }
    }
}
