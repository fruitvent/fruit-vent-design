﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesignDemo.Model
//  
//  Function： N/A
//  Name： Student
//  
//  Ver       Time                     Author
//  0.10      2021/6/25 12:19:29      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FruitVentDesignDemo.Model
{
    public class Student : NotificationObject
    {
        public Student() { }
        public Student(string id, string name, int age) 
        {
            Id = id;
            Name = name;
            Age = age;
        }

        public Student(string id, string name, int age, string grade)
        {
            Id = id;
            Name = name;
            Age = age;
            Grade = grade;
        }

        private string _id;
        public string Id
        {
            get => _id;
            set
            {
                if (_id != value)
                {
                    _id = value;
                    RaisePropertyChanged(nameof(Id));
                }
            }
        }

        private string _name;
        [Required(ErrorMessage = "姓名不能为空")]
        public string Name
        {
            get => _name;
            set
            {
                value = value?.Trim();
                if (_name != value)
                {
                    _name = value;
                    RaisePropertyChanged(nameof(Name));
                }
            }
        }

        private int _age;
        public int Age
        {
            get => _age;
            set
            {
                if (_age != value)
                {
                    _age = value;
                    RaisePropertyChanged(nameof(Age));
                }
            }
        }

        private string _grade;
        [Required(ErrorMessage = "年级不能为空")]
        public string Grade
        {
            get => _grade;
            set
            {
                if (_grade != value)
                {
                    _grade = value;
                    RaisePropertyChanged(nameof(Grade));
                }
            }
        }
    }
}
