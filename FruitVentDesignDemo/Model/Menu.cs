﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesignDemo.Model
//  
//  Function： N/A
//  Name： MenuTree
//  
//  Ver       Time                     Author
//  0.10      2021/6/23 18:45:33      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using FruitVentMVVM.Command;
using System.Collections.Generic;

namespace FruitVentDesignDemo.Model
{
    public class Menu
    {
        public string Id { set; get; }
        public string ParentId { set; get; }
        public string Name { set; get; }
        public string Icon { set; get; }
        public List<Menu> Children { set; get; }

        public Menu() { }

        public Menu(string id, string parentId, string name, string icon)
        {
            Id = id;
            ParentId = parentId;
            Name = name;
            Icon = icon;
        }
    }
}
