﻿using FruitVentDesign.Models;
using FruitVentDesign.Utils;
using FruitVentDesign.UtilWindows;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace FruitVentDesignDemo.View
{
    /// <summary>
    /// FunctionalWindowPage.xaml 的交互逻辑
    /// </summary>
    public partial class FunctionalWindowView : UserControl
    {
        private Window _win = null;

        public FunctionalWindowView()
        {
            InitializeComponent();
            Loaded += FunctionalWindowPage_Loaded;
        }

        private void FunctionalWindowPage_Loaded(object sender, RoutedEventArgs e)
        {
            _win = Window.GetWindow(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReportMessage_Click(object sender, RoutedEventArgs e)
        {
            FrontEndUtils.ReportMessage("ReportMessage", _win);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReportError_Click(object sender, RoutedEventArgs e)
        {
            FrontEndUtils.ReportError("ReportError", new System.Exception("异常"), _win);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConfirmOperation_Click(object sender, RoutedEventArgs e)
        {
            FrontEndUtils.ConfirmOperation("ConfirmOperation", _win);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AskforEnter_Click(object sender, RoutedEventArgs e)
        {
            FrontEndUtils.AskforEnter(_win);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NoticeMessage_Click(object sender, RoutedEventArgs e)
        {
            FrontEndUtils.NoticeMessage("NoticeMessage");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PopupMessage_Click(object sender, RoutedEventArgs e)
        {
            FrontEndUtils.PopupMessage("PopupMessage", _win);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReportReadOnlyMessage_Click(object sender, RoutedEventArgs e)
        {
            FrontEndUtils.ReportReadOnlyMessage("ReportReadOnlyMessage", _win);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WaitingOperation_Click(object sender, RoutedEventArgs e)
        {
            FrontEndUtils.WaitingOperation("WaitingOperation", () =>
            {
                for (int i = 0; i < 100000; i++)
                {
                    for (int j = 0; j < 10000; j++)
                    {

                    }
                }
            }, true, _win);
        }

        private void PrintImage_Click(object sender, RoutedEventArgs e)
        {
            List<string> lists = new List<string>
            {
                $"{System.AppDomain.CurrentDomain.BaseDirectory}/img/1.jpg",
                $"{System.AppDomain.CurrentDomain.BaseDirectory}/img/2.jpg",
                $"{System.AppDomain.CurrentDomain.BaseDirectory}/img/3.jpg"
            };
            PrintImageWindow printImageWindow = new PrintImageWindow(lists, "复印件");
            printImageWindow.ShowDialog();
        }

        private void Tip_Click(object sender, RoutedEventArgs e)
        {
            _ = FrontEndUtils.TipAsync("提示一下");
        }
    }
}
