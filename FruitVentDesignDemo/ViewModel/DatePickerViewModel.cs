﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesignDemo.ViewModel
//  
//  Function： N/A
//  Name： DatePickerViewModel
//  
//  Ver       Time                     Author
//  0.10      2023/2/13 16:07:05                   Benedict Deng
//
//  Description:
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

using FruitVentMVVM;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace FruitVentDesignDemo.ViewModel
{
    public class DatePickerViewModel : ViewModelBase
    {
        private DateTime? _date = DateTime.Now;
        public DateTime? Date
        {
            get => _date;
            set
            {
                if(_date != value)
                {
                    _date = value;
                    OnPropertyChanged(nameof(Date));

                    if(Date.HasValue)
                    {
                        Trace.WriteLine($"Date:{Date.Value.ToString("yyyy-MM-dd HH:mm:ss")}");
                    }
                }
            }
        }
    }
}
