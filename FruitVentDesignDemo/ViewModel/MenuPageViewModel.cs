﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesignDemo.ViewModel
//  
//  Function： N/A
//  Name： MenuPageViewModel
//  
//  Ver       Time                     Author
//  0.10      2021/6/29 12:35:19      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using FruitVentDesignDemo.Model;
using FruitVentMVVM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace FruitVentDesignDemo.ViewModel
{
    public class MenuPageViewModel : ViewModelBase
    {
        public MenuPageViewModel()
        {
            List<Menu> datas = new List<Menu>
            {
                new Menu("0-0", "0", "Option 1",""),
                new Menu("0-0-0", "0-0", "Option 2",""),
                new Menu("0-0-0-0", "0-0-0", "Option 3",""),
                new Menu("0-0-0-1", "0-0-0", "Option 4",""),
                new Menu("0-0-1", "0-0", "Option 5",""),
                new Menu("0-0-1-0", "0-0-1", "Option 6",""),
                new Menu("0-1", "0", "Option 7",""),
                new Menu("0-1-0", "0-1", "Option 8",""),
                new Menu("0-3", "0", "Option 9","")
            };
            List<Menu> menus = ConversionList(datas, "0", "Id", "ParentId", "Name", "Icon");
            Menus = new ObservableCollection<Menu>(menus);
        }

        /// <summary>
        /// 公用递归(反射转换List)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="allList"></param>
        /// <param name="parentId"></param>
        /// <param name="idField"></param>
        /// <param name="parentIdField"></param>
        /// <param name="nameField"></param>
        /// <returns></returns>
        public List<Menu> ConversionList<T>(List<T> allList, string parentId, string idField, string parentIdField, string nameField, string iconField)
        {
            List<Menu> list = new List<Menu>();
            foreach (var item in allList)
            {
                Menu model = new Menu();
                foreach (System.Reflection.PropertyInfo p in item.GetType().GetProperties())
                {
                    if (p.Name == idField)
                    {
                        model.Id = p.GetValue(item).ToString();
                    }
                    if (p.Name == parentIdField)
                    {
                        model.ParentId = p.GetValue(item).ToString();
                    }
                    if (p.Name == nameField)
                    {
                        model.Name = p.GetValue(item).ToString();
                    }
                    if(p.Name == iconField)
                    {
                        model.Icon = p.GetValue(item).ToString();
                    }
                }

                list.Add(model);
            }
            return OperationParentData(list, parentId);
        }

        /// <summary>
        /// 公用递归(处理递归最父级数据)
        /// </summary>
        /// <param name="treeDataList">树形列表数据</param>
        /// <param name="parentId">父级Id</param>
        /// <returns></returns>
        public List<Menu> OperationParentData(List<Menu> treeDataList, string parentId)
        {
            var data = treeDataList.Where(x => x.ParentId == parentId);
            List<Menu> list = new List<Menu>();
            foreach (var item in data)
            {
                OperationChildData(treeDataList, item);
                list.Add(item);
            }
            return list;
        }

        /// <summary>
        /// 公用递归(递归子级数据)
        /// </summary>
        /// <param name="treeDataList">树形列表数据</param>
        /// <param name="parentItem">父级model</param>
        public void OperationChildData(List<Menu> treeDataList, Menu parentItem)
        {
            var subItems = treeDataList.Where(ee => ee.ParentId == parentItem.Id).ToList();
            if (subItems.Count != 0)
            {
                parentItem.Children = new List<Menu>();
                parentItem.Children.AddRange(subItems);
                foreach (var subItem in subItems)
                {
                    OperationChildData(treeDataList, subItem);
                }
            }
        }

        /// <summary>
        /// trees
        /// </summary>
        private ObservableCollection<Menu> _menus;
        public ObservableCollection<Menu> Menus
        {
            get { return _menus; }
            set
            {
                if (_menus != value)
                {
                    _menus = value;
                    OnPropertyChanged(nameof(Menus));
                }
            }
        }
    }
}
