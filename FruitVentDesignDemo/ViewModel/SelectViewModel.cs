﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesignDemo.ViewModel
//  
//  Function： N/A
//  Name： SelectPageViewModel
//  
//  Ver       Time                     Author
//  0.10      2021/6/23 20:34:00      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using FruitVentDesign.Utils;
using FruitVentDesignDemo.Model;
using FruitVentDesignDemo.Util;
using FruitVentMVVM;
using System.Collections.Generic;
using System.Diagnostics;

namespace FruitVentDesignDemo.ViewModel
{
    public class SelectViewModel : ViewModelBase
    {
        public SelectViewModel()
        {
            Trace.WriteLine("SelectPageViewModel");
        }

        private string _selectedListItem;
        public string SelectedListItem
        {
            get { return _selectedListItem; }
            set
            {
                if(_selectedListItem != value)
                {
                    _selectedListItem = value;
                    if(_selectedListItem != null)
                    {
                        Trace.WriteLine(_selectedListItem);
                    }
                    OnPropertyChanged(nameof(SelectedListItem));
                }
            }
        }

        private List<string> _list;
        public List<string> List
        {
            get
            {
                List<string> list = new List<string>
                {
                    "张三",
                    "李四",
                    "王五",
                    "王二狗子"
                };
                _list = list;
                return _list;
            }
            set
            {
                if (_list != value)
                {
                    _list = value;
                    OnPropertyChanged(nameof(List));
                }
            }
        }

        private Student _selectedStudent;
        public Student SelectedStudernt
        {
            get { return _selectedStudent; }
            set
            {
                if(_selectedStudent != value)
                {
                    _selectedStudent = value;
                    if(_selectedStudent != null)
                    {
                        Trace.WriteLine(_selectedStudent.Name);
                    }
                    OnPropertyChanged(nameof(SelectedStudernt));
                }
            }
        }

        private List<Student> _students;
        public List<Student> Students
        {
            get 
            {
                List<Student> students = new List<Student>();
                IdWorker idWorker = new IdWorker(0, 0);
                students.Add(new Student(idWorker.NextId().ToString(), "小明", 10));
                students.Add(new Student(idWorker.NextId().ToString(), "小虎", 10));
                students.Add(new Student(idWorker.NextId().ToString(), "小红", 10));
                students.Add(new Student(idWorker.NextId().ToString(), "大王", 10));
                _students = students;
                return _students; 
            }
            set
            {
                if(_students != value)
                {
                    _students = value;
                    OnPropertyChanged(nameof(Students));
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private List<Office> _offices;
        public List<Office> Offices
        {
            get
            {
                List<Office> offices = new List<Office>();
                IdWorker idWorker = new IdWorker(0, 0);
                offices.Add(new Office(idWorker.NextId().ToString(), "学生"));
                offices.Add(new Office(idWorker.NextId().ToString(), "老师"));
                _offices = offices;
                return _offices;
            }
            set
            {
                if (_offices != value)
                {
                    _offices = value;
                    OnPropertyChanged(nameof(Offices));
                }
            }
        }
    }
}
