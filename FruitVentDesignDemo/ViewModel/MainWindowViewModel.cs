﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesignDemo.ViewModel
//  
//  Function： N/A
//  Name： MainWindowViewModel
//  
//  Ver       Time                     Author
//  0.10      2023/2/9 17:31:28                   Benedict Deng
//
//  Description:
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

using FruitVentMVVM;
using FruitVentMVVM.Command;
using System;
using System.Collections.Generic;
using System.Text;

namespace FruitVentDesignDemo.ViewModel
{
    public class MainWindowViewModel : ViewModelBase
    {
        public MainWindowViewModel()
        {
            Title = "ColorPicker";
            PageViewModel = new ColorPickerViewModel();
        }

        /// <summary>
        /// 展示页面
        /// </summary>
        private ViewModelBase _pageViewModel;
        public ViewModelBase PageViewModel
        {
            get => _pageViewModel;
            set
            {
                if (_pageViewModel != value)
                {
                    _pageViewModel = value;
                    OnPropertyChanged(nameof(PageViewModel));
                }
            }
        }

        private string _title;
        public string Title
        {
            get => _title;
            set
            {
                if (_title != value)
                {
                    _title = value;
                    OnPropertyChanged(nameof(Title));
                }
            }
        }

        /// <summary>
        /// 切换页面
        /// </summary>
        private RelayCommand<string> _changePageCommand;
        public RelayCommand<string> ChangePageCommand
        {
            get
            {
                if (_changePageCommand == null)
                {
                    _changePageCommand = new RelayCommand<string>(OnChangePage);
                }
                return _changePageCommand;
            }
        }

        private void OnChangePage(string title)
        {
            if (Title == title) { return; }
            switch (title)
            {
                case "Button":
                    PageViewModel = new ButtonViewModel();
                    Title = title;
                    break;
                case "Radio":
                    PageViewModel = new RadioViewModel();
                    Title = title;
                    break;
                case "CheckBox":
                    PageViewModel = new CheckBoxViewModel();
                    Title = title;
                    break;
                case "ColorPicker":
                    PageViewModel = new ColorPickerViewModel();
                    Title = title;
                    break;
                case "Input":
                    PageViewModel = new InputViewModel();
                    Title = title;
                    break;
                case "Select":
                    PageViewModel = new SelectViewModel();
                    Title = title;
                    break;
                case "TreeSelect":
                    PageViewModel = new TreeSelectViewModel();
                    Title = title;
                    break;
                case "DatePicker":
                    PageViewModel = new DatePickerViewModel();
                    Title = title;
                    break;
                case "Form":
                    PageViewModel = new FormViewModel();
                    Title = title;
                    break;
                case "Tree":
                    PageViewModel = new TreeViewModel();
                    Title = title;
                    break;
                case "Table":
                    PageViewModel = new TableViewModel();
                    Title = title;
                    break;
                case "FunctionalWindow":
                    PageViewModel = new FunctionalWindowViewModel();
                    Title = title;
                    break;
                default:
                    break;
            }
        }
    }
}
