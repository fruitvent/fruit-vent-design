﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesignDemo.ViewModel
//  
//  Function： N/A
//  Name： ColorPickerViewModel
//  
//  Ver       Time                     Author
//  0.10      2023/2/16 9:18:06                   Benedict Deng
//
//  Description:
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

using FruitVentMVVM;
using System;
using System.Collections.Generic;
using System.Text;

namespace FruitVentDesignDemo.ViewModel
{
    public class ColorPickerViewModel : ViewModelBase
    {
        public ColorPickerViewModel()
        {
        }
    }
}
