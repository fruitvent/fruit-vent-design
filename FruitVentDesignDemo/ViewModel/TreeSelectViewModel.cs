﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesignDemo.ViewModel
//  
//  Function： N/A
//  Name： TreeSelectPageViewModel
//  
//  Ver       Time                     Author
//  0.10      2021/6/25 8:56:31      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using FruitVentDesignDemo.Model;
using FruitVentMVVM;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;

namespace FruitVentDesignDemo.ViewModel
{
    public class TreeSelectViewModel : ViewModelBase
    {
        public TreeSelectViewModel()
        {
            List<TreeModel> datas = new List<TreeModel>
            {
                new TreeModel("0-0", "0", "parent1"),
                new TreeModel("0-0-0", "0-0", "parent1-0"),
                new TreeModel("0-0-0-0", "0-0-0", "leaf"),
                new TreeModel("0-0-0-1", "0-0-0", "leaf"),
                new TreeModel("0-0-1", "0-0", "parent1-1"),
                new TreeModel("0-0-1-0", "0-0-1", "leaf"),
                new TreeModel("0-1", "0", "parent2"),
                new TreeModel("0-1-0", "0-1", "leaf")
            };
            List<TreeModel> trees = ConversionList(datas, "0", "Id", "PId", "Name");
            Trees = new ObservableCollection<TreeModel>(trees);
        }

        /// <summary>
        /// 公用递归(反射转换List)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="allList"></param>
        /// <param name="parentId"></param>
        /// <param name="idField"></param>
        /// <param name="parentIdField"></param>
        /// <param name="nameField"></param>
        /// <returns></returns>
        public List<TreeModel> ConversionList<T>(List<T> allList, string parentId, string idField, string parentIdField, string nameField)
        {
            List<TreeModel> list = new List<TreeModel>();
            foreach (T item in allList)
            {
                TreeModel model = new TreeModel();
                foreach (System.Reflection.PropertyInfo p in item.GetType().GetProperties())
                {
                    if (p.Name == idField)
                    {
                        model.Id = p.GetValue(item).ToString();
                    }
                    if (p.Name == parentIdField)
                    {
                        model.PId = p.GetValue(item).ToString();
                    }
                    if (p.Name == nameField)
                    {
                        model.Name = p.GetValue(item).ToString();
                    }
                }

                list.Add(model);
            }
            return OperationParentData(list, parentId);
        }

        /// <summary>
        /// 公用递归(处理递归最父级数据)
        /// </summary>
        /// <param name="treeDataList">树形列表数据</param>
        /// <param name="parentId">父级Id</param>
        /// <returns></returns>
        public List<TreeModel> OperationParentData(List<TreeModel> treeDataList, string parentId)
        {
            IEnumerable<TreeModel> data = treeDataList.Where(x => x.PId == parentId);
            List<TreeModel> list = new List<TreeModel>();
            foreach (TreeModel item in data)
            {
                OperationChildData(treeDataList, item);
                list.Add(item);
            }
            return list;
        }

        /// <summary>
        /// 公用递归(递归子级数据)
        /// </summary>
        /// <param name="treeDataList">树形列表数据</param>
        /// <param name="parentItem">父级model</param>
        public void OperationChildData(List<TreeModel> treeDataList, TreeModel parentItem)
        {
            List<TreeModel> subItems = treeDataList.Where(ee => ee.PId == parentItem.Id).ToList();
            if (subItems.Count != 0)
            {
                parentItem.Children = new List<TreeModel>();
                parentItem.Children.AddRange(subItems);
                foreach (TreeModel subItem in subItems)
                {
                    OperationChildData(treeDataList, subItem);
                }
            }
        }

        /// <summary>
        /// trees
        /// </summary>
        private ObservableCollection<TreeModel> _trees;
        public ObservableCollection<TreeModel> Trees
        {
            get => _trees;
            set
            {
                if (_trees != value)
                {
                    _trees = value;
                    OnPropertyChanged(nameof(Trees));
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private TreeModel _bSelectedTreeModel;
        public TreeModel BSelectedTreeModel
        {
            get => _bSelectedTreeModel;
            set
            {
                if (_bSelectedTreeModel != value)
                {
                    _bSelectedTreeModel = value;
                    if (_bSelectedTreeModel != null)
                    {
                        Trace.WriteLine(_bSelectedTreeModel.Name);
                    }
                    else
                    {
                        Trace.WriteLine("_bSelectedTreeModel is null");
                    }
                    OnPropertyChanged(nameof(BSelectedTreeModel));
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private TreeModel _selectedTreeModel;
        public TreeModel SelectedTreeModel
        {
            get => _selectedTreeModel;
            set
            {
                if (_selectedTreeModel != value)
                {
                    _selectedTreeModel = value;
                    if (_selectedTreeModel != null)
                    {
                        Trace.WriteLine(_selectedTreeModel.Name);
                    }
                    else
                    {
                        Trace.WriteLine("_selectedTreeModel is null");
                    }
                    OnPropertyChanged(nameof(SelectedTreeModel));
                }
            }
        }
    }
}
