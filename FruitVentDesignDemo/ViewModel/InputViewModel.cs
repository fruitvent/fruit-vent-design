﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesignDemo.ViewModel
//  
//  Function： N/A
//  Name： InputPageViewModel
//  
//  Ver       Time                     Author
//  0.10      2021/6/24 18:10:43      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using FruitVentDesign.Controls;
using FruitVentDesign.Utils;
using FruitVentMVVM;
using FruitVentMVVM.Command;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Input;

namespace FruitVentDesignDemo.ViewModel
{
    public class InputViewModel : ViewModelBase
    {
        private List<string> _list;
        public List<string> List
        {
            get
            {
                List<string> list = new List<string>
                {
                    "张三",
                    "李四",
                    "王五"
                };
                _list = list;
                return _list;
            }
            set
            {
                if (_list != value)
                {
                    _list = value;
                    OnPropertyChanged(nameof(List));
                }
            }
        }

        private RelayCommand<string> _getSearchValueCommand;
        public RelayCommand<string> GetSearchValueCommand
        {
            get
            {
                if(_getSearchValueCommand == null)
                {
                    _getSearchValueCommand = new RelayCommand<string>(OnGetSearchValue);
                }
                return _getSearchValueCommand;
            }
        }

        private void OnGetSearchValue(string obj)
        {
            Trace.WriteLine("search");
            FrontEndUtils.ReportMessage(obj);
        }

        private RelayCommand<object> _searchCommand;
        public RelayCommand<object> SearchCommand
        {
            get
            {
                if (_searchCommand == null)
                {
                    _searchCommand = new RelayCommand<object>(OnSearch);
                }
                return _searchCommand;
            }
        }

        private void OnSearch(object obj)
        {
            if (obj is PromptInput input)
            {
                input.Execute();
            }
        }
    }
}
