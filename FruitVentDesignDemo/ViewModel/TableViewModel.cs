﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesignDemo.ViewModel
//  
//  Function： N/A
//  Name： TablePageViewModel
//  
//  Ver       Time                     Author
//  0.10      2021/6/28 10:36:50      FruitVent
//
//  此代码版权归作者本人FruitVent所有
//  源代码使用协议遵循本仓库的开源协议及附加协议，若本仓库没有设置，则按MIT开源协议授权
//  CSDN博客：https://blog.csdn.net/weixin_39552347
//  源代码仓库：https://gitee.com/fruitvent
//  感谢您的下载和使用
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
using FruitVentDesign.Models;
using FruitVentDesignDemo.Model;
using FruitVentDesignDemo.Util;
using FruitVentMVVM;
using FruitVentMVVM.Command;
using System.Collections.Generic;
using System.Diagnostics;

namespace FruitVentDesignDemo.ViewModel
{
    public class TableViewModel : ViewModelBase
    {
        public TableViewModel()
        {
            Trace.WriteLine("TablePageViewModel");
        }

        /// <summary>
        /// student list
        /// </summary>
        private List<Student> _students;
        public List<Student> Students
        {
            get
            {
                IdWorker idWorker = new IdWorker(0, 0);
                List<Student> students = new List<Student>
                {
                    new Student(idWorker.NextId().ToString(), "小明", 7, "一年级"),
                    new Student(idWorker.NextId().ToString(), "小陈", 6, "一年级"),
                    new Student(idWorker.NextId().ToString(), "小郑", 7, "一年级"),
                    new Student(idWorker.NextId().ToString(), "小芳", 7, "一年级"),
                    new Student(idWorker.NextId().ToString(), "小虎", 8, "二年级"),
                    new Student(idWorker.NextId().ToString(), "小红", 8, "二年级"),
                    new Student(idWorker.NextId().ToString(), "小王", 9, "三年级"),
                    new Student(idWorker.NextId().ToString(), "小蓝", 9, "三年级"),
                    new Student(idWorker.NextId().ToString(), "小华", 10, "四年级"),
                    new Student(idWorker.NextId().ToString(), "小小", 11, "五年级"),
                    new Student(idWorker.NextId().ToString(), "小张", 12, "六年级")
                };
                _students = students;
                return _students;
            }
            set
            {
                if (_students != value)
                {
                    _students = value;
                    OnPropertyChanged(nameof(Students));
                }
            }
        }

        /// <summary>
        /// grade filter items
        /// </summary>
        public IEnumerable<FilterItem> GradeFilterItems
        {
            get
            {
                List<FilterItem> items = new List<FilterItem>
                {
                    new FilterItem() { DisplayText = "一年级" },
                    new FilterItem() { DisplayText = "二年级" },
                    new FilterItem() { DisplayText = "三年级" },
                    new FilterItem() { DisplayText = "四年级" },
                    new FilterItem() { DisplayText = "五年级" },
                    new FilterItem() { DisplayText = "六年级" },
                    new FilterItem() { DisplayText = "七年级" },
                    new FilterItem() { DisplayText = "八年级" },
                    new FilterItem() { DisplayText = "九年级" }
                };
                return items;
            }
        }

        /// <summary>
        /// get filter sources command
        /// </summary>
        private RelayCommand<List<object>> _getFilterSources;
        public RelayCommand<List<object>> GetFilterSources
        {
            get
            {
                if(_getFilterSources == null)
                {
                    _getFilterSources = new RelayCommand<List<object>>(OnGetFilterSources);
                }
                return _getFilterSources;
            }
        }

        /// <summary>
        /// get filter sources
        /// </summary>
        /// <param name="obj"></param>
        private void OnGetFilterSources(List<object> obj)
        {
            if(obj != null)
            {
                foreach(var item in obj)
                {
                    Student student = (Student)item;
                    Trace.WriteLine(student.Name);
                }
            }
        }

        public int[] PageSizeOptions { get; set; } = new int[] { 3, 6, 9, 12 };
    }
}
