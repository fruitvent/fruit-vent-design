﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesignDemo.ViewModel
//  
//  Function： N/A
//  Name： FunctionalWindowViewModel
//  
//  Ver       Time                     Author
//  0.10      2023/2/13 16:14:28                   Benedict Deng
//
//  Description:
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

using FruitVentMVVM;
using System;
using System.Collections.Generic;
using System.Text;

namespace FruitVentDesignDemo.ViewModel
{
    public class FunctionalWindowViewModel : ViewModelBase
    {
    }
}
