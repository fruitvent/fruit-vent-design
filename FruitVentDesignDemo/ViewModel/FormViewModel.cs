﻿//------------------------------------------------------------------------------
//  Namespace: FruitVentDesignDemo.ViewModel
//  
//  Function： N/A
//  Name： FormPageViewModel
//  
//  Ver       Time                     Author
//  0.10      2021/9/18 9:26:31                   Benedict Deng
//
//  Description:
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

using FruitVentDesignDemo.Model;
using FruitVentMVVM;
using FruitVentMVVM.Command;
using System.Collections.Generic;
using System.Diagnostics;

namespace FruitVentDesignDemo.ViewModel
{
    public class FormViewModel : ViewModelBase
    {
        public FormViewModel()
        {
            Trace.WriteLine("FormPageViewModel");
            Student = new Student();
        }

        private Student _student;
        public Student Student
        {
            get => _student;
            set
            {
                if(_student != value)
                {
                    _student = value;
                    OnPropertyChanged(nameof(Student));
                }
            }
        }

        private List<string> _grades;
        public List<string> Grades
        {
            get
            {
                _grades = new List<string>();
                _grades.Add("一年级");
                _grades.Add("二年级");
                _grades.Add("三年级");
                _grades.Add("四年级");
                _grades.Add("五年级");
                _grades.Add("六年级");
                return _grades;
            }
            set
            {
                if(_grades!= value)
                {
                    _grades = value;
                    OnPropertyChanged(nameof(Grades));
                }
            }
        }

        private RelayCommand<List<string>> _validateFieldsCommand;
        public RelayCommand<List<string>> ValidateFieldsCommand
        {
            get
            {
                if(_validateFieldsCommand == null)
                {
                    _validateFieldsCommand = new RelayCommand<List<string>>(OnValidateFields);
                }
                return _validateFieldsCommand;
            }
        }

        private void OnValidateFields(List<string> messages)
        {
            foreach(string msg in messages)
            {
                Trace.WriteLine(msg);
            }
        }
    }
}
