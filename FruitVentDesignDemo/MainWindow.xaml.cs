﻿using FruitVentDesign.Enums;
using FruitVentDesign.Utils;
using FruitVentDesignDemo.View;
using FruitVentDesignDemo.ViewModel;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace FruitVentDesignDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : FruitVentDesign.Windows.Window
    {
        /// <summary>
        /// 窗体主题颜色
        /// </summary>
        public ThemeKind CurrenTheme { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            switch (FruitVentDesign.Configurations.Cinfiguration.ThemeColor)
            {
                case "Blue":
                    ThemeAssist.ChangeTheme(ThemeKind.Blue);
                    CurrenTheme = ThemeKind.Blue;
                    break;
                default:
                    break;
            }

            DataContext = new MainWindowViewModel();
        }
    }
}
